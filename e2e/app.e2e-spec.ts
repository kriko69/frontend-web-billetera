import { BilleteraWebPage } from './app.po';

describe('billetera-web App', () => {
  let page: BilleteraWebPage;

  beforeEach(() => {
    page = new BilleteraWebPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
