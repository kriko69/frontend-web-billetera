import { TestBed, inject } from '@angular/core/testing';

import { AdministradorServiceService } from './administrador-service.service';

describe('AdministradorServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AdministradorServiceService]
    });
  });

  it('should ...', inject([AdministradorServiceService], (service: AdministradorServiceService) => {
    expect(service).toBeTruthy();
  }));
});
