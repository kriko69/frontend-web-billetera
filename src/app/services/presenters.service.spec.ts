import { TestBed, inject } from '@angular/core/testing';

import { PresentersService } from './presenters.service';

describe('PresentersService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PresentersService]
    });
  });

  it('should ...', inject([PresentersService], (service: PresentersService) => {
    expect(service).toBeTruthy();
  }));
});
