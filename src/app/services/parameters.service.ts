import { Injectable } from '@angular/core';

@Injectable()
export class ParametersService {

  public tipoRegistroUsuario:string;
  public estudianteEditar:any=null;
  public administradorEditar:any=null;
  public token:string;
  public rol:string;
  public usuarioLogin:any;
  public inicioSesion:boolean;
  public carnetResetPass:number;
  public fotoAdministrador='http://157.245.140.17/imagenes/usuarios/administradores/';
  public fotoSuperAdministrador='http://157.245.140.17/imagenes/usuarios/superadmin/';
  carnetAdmin:number;
  public gestion;
  public mensualidad;
  constructor() { 

  }

  getMensualidad()
  {
    return this.mensualidad;
  }
  setMensualidad(m:any)
  {
    this.mensualidad=m;
  }
  getGestion()
  {
    return this.gestion;
  }
  setGestion(g:any)
  {
    this.gestion=g;
  }
  getCarnetResetPass()
  {
    return this.carnetResetPass;
  }
  setCarnetResetPass(c:number)
  {
    this.carnetResetPass=c;
  }
  getCarnetAdmin()
  {
    return this.carnetAdmin;
  }
  setCarnetAdmin(c:number)
  {
    this.carnetAdmin=c;
  }
  getInicioSesion(){
    return this.inicioSesion;
  }
  setInicioSesion(i:boolean){
    this.inicioSesion=i;
  }
  getUsuarioLogin()
  {
    return this.usuarioLogin;
  }
  setUsuarioLogin(u:any)
  {
    this.usuarioLogin=u;
  }
  getRol()
  {
    return this.rol;
  }
  setRol(r:string)
  {
    this.rol=r;
  }
  getToken()
  {
    return this.token;
  }
  setToken(t:string)
  {
    this.token=t;
  }
  getAdministradorEditar()
  {
    return this.administradorEditar;
  }
  setAdministradorEditar(a:any)
  {
    this.administradorEditar=a;
  }
  getEstudianteEditar()
  {
    return this.estudianteEditar;
  }
  setEstudianteEditar(e:any)
  {
    this.estudianteEditar=e;
  }
  getTipoRegistroUsuario()
  {
    return this.tipoRegistroUsuario;
  }
  setTipoRegistroUsuario(tipo:string)
  {
    this.tipoRegistroUsuario=tipo;
  }

}
