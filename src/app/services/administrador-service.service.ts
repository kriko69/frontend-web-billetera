import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {Http,Headers} from '@angular/http';
import {map} from 'rxjs/operators';
@Injectable()
export class AdministradorServiceService {

  ServidorUrl='http://157.245.140.17/api/';
  //ServidorUrl="http://localhost:8000/api/";
  constructor(public http:HttpClient,
    public http2:Http) {

  }

  login(carnet:number,password:string){
    let data={
      "carnet":carnet,
      "password":password
    };

    let header : any = new HttpHeaders({'Content-Type': 'application/json'}),
    opsi   : any = JSON.stringify(data);
    console.log('opsi',opsi);

    return this.http.post(this.ServidorUrl+'login', opsi, header);
  }
  editarPerifl(token:string,estudiante)
  {
    if(estudiante.carrera=='')
    {
      estudiante.carrera='sin carrera';
    }
    let data={
      "usuario_id":estudiante.usuario_id,
      "nombre":estudiante.nombre,
      "apellidos":estudiante.apellidos,
      "correo":estudiante.correo,
      "telefono":estudiante.telefono,
      "fecha_nacimiento":estudiante.fecha_nacimiento,
      "carrera":estudiante.carrera
    };
    let header : any = new Headers(),
    opsi   : any = JSON.stringify(data);
    header.append("Authorization",token);
    header.append("Content-Type","application/json");
    return this.http2.post(this.ServidorUrl+'usuario/editar',opsi,{ headers:header});
  }

  activarUsuario(token:string,usuario_id:number)
  {
    let data={
      "usuario_id":usuario_id
    };
    let header : any = new Headers(),
    opsi   : any = JSON.stringify(data);
    header.append("Authorization",token);
    header.append("Content-Type","application/json");
    return this.http2.put(this.ServidorUrl+'usuario/activar',opsi,{ headers:header});
  }

  eliminarUsuario(token:string,usuario_id:number)
  {
    let data={
      "usuario_id":usuario_id
    };
    let header : any = new Headers(),
    opsi   : any = JSON.stringify(data);
    header.append("Authorization",token);
    header.append("Content-Type","application/json");
    return this.http2.put(this.ServidorUrl+'usuario/eliminar',opsi,{ headers:header});
  }

  perfil(token:string)
  {
    let header : any = new Headers();
    header.append("Authorization",token);
    return this.http2.get(this.ServidorUrl+'perfil',{ headers:header});
  }

  obtenerListaUsuarios(token:string)
  {
    let header : any = new Headers();
    header.append("Authorization",token);
    return this.http2.get(this.ServidorUrl+'usuarios',{ headers:header});
  }

  editarMiPerifl(token:string,estudiante)
  {
    let data={
      "nombre":estudiante.nombre,
      "apellidos":estudiante.apellidos,
      "correo":estudiante.correo,
      "telefono":estudiante.telefono,
      "fecha_nacimiento":estudiante.fecha_nacimiento,
      "carrera":estudiante.carrera
    };
    let header : any = new Headers(),
    opsi   : any = JSON.stringify(data);
    header.append("Authorization",token);
    header.append("Content-Type","application/json");
    return this.http2.put(this.ServidorUrl+'perfil',opsi,{ headers:header});
  }

  cambiarMiPassword(token:string,carnet:number,pass1:string,pass2:string)
  {
    let data={
      'carnet': carnet,
      'password':pass1,
      'repetir_password':pass2
    };
    let header : any = new Headers(),
    opsi   : any = JSON.stringify(data);
    header.append("Authorization",token);
    header.append("Content-Type","application/json");
    return this.http2.post(this.ServidorUrl+'reset/password/cambiar',opsi,{ headers:header});
  }
  
  registrarEstudiante(token:string,info:any)
  {
    let data={
      "nombre":info.nombre,
      "apellidos":info.apellidos,
      "carnet":info.carnet,
      "correo":info.correo,
      "telefono":info.telefono,
      "fecha_nacimiento":info.fecha_nacimiento,
      "carrera":info.carrera
    };
    let header : any = new Headers(),
    opsi   : any = JSON.stringify(data);
    header.append("Authorization",token);
    header.append("Content-Type","application/json");
    return this.http2.post(this.ServidorUrl+'registrar-estudiante',opsi,{ headers:header});
  }
  
  registrarCuentaEstudiante(token:string,usuario_id:number,pass:string)
  {
    let data={
      "saldo":0,
      "usuario_id":usuario_id,
      "password-default":pass
    };
    let header : any = new Headers(),
    opsi   : any = JSON.stringify(data);
    header.append("Authorization",token);
    header.append("Content-Type","application/json");
    return this.http2.post(this.ServidorUrl+'cuenta/estudiante',opsi,{ headers:header});
  }

  listarCuentas(token:string)
  {
    let header : any = new Headers();
    header.append("Authorization",token);
    return this.http2.get(this.ServidorUrl+'cuentas',{ headers:header});
  }

  eliminarCuenta(token:string,cuenta_id:number)
  {
    let data={
      "cuenta_id":cuenta_id
    };
    let header : any = new Headers(),
    opsi   : any = JSON.stringify(data);
    header.append("Authorization",token);
    header.append("Content-Type","application/json");
    return this.http2.put(this.ServidorUrl+'cuenta/eliminar',opsi,{ headers:header});
  }
  activarCuenta(token:string,cuenta_id:number)
  {
    let data={
      "cuenta_id":cuenta_id
    };
    let header : any = new Headers(),
    opsi   : any = JSON.stringify(data);
    header.append("Authorization",token);
    header.append("Content-Type","application/json");
    return this.http2.put(this.ServidorUrl+'cuenta/activar',opsi,{ headers:header});
  }

  listarNegocios(token:string)
  {
    let header : any = new Headers();
    header.append("Authorization",token);
    return this.http2.get(this.ServidorUrl+'negocios/admin',{ headers:header});
  }
  eliminarNegocio(token:string,negocio_id:number)
  {
    let data={
      "negocio_id":negocio_id
    };
    let header : any = new Headers(),
    opsi   : any = JSON.stringify(data);
    header.append("Authorization",token);
    header.append("Content-Type","application/json");
    return this.http2.put(this.ServidorUrl+'negocio/eliminar',opsi,{ headers:header});
  }
  activarNegocio(token:string,negocio_id:number)
  {
    let data={
      "negocio_id":negocio_id
    };
    let header : any = new Headers(),
    opsi   : any = JSON.stringify(data);
    header.append("Authorization",token);
    header.append("Content-Type","application/json");
    return this.http2.put(this.ServidorUrl+'negocio/activar',opsi,{ headers:header});
  }

  aceptarAfiliacionNegocio(token:string,negocio_id:number)
  {
    let data={
      "negocio_id":negocio_id
    };
    let header : any = new Headers(),
    opsi   : any = JSON.stringify(data);
    header.append("Authorization",token);
    header.append("Content-Type","application/json");
    return this.http2.put(this.ServidorUrl+'negocio/afiliacion/aceptar',opsi,{ headers:header});
  }

  rechazarAfiliacionNegocio(token:string,negocio_id:number,usuario_id:number)
  {
    let data={
      "negocio_id":negocio_id,
      "usuario_id":usuario_id
    };
    let header : any = new Headers(),
    opsi   : any = JSON.stringify(data);
    header.append("Authorization",token);
    header.append("Content-Type","application/json");
    return this.http2.put(this.ServidorUrl+'negocio/afiliacion/rechazar',opsi,{ headers:header});
  }

  obtenerListaUsuariosAdministradores(token:string)
  {
    let header : any = new Headers();
    header.append("Authorization",token);
    return this.http2.get(this.ServidorUrl+'usuarios/admins',{ headers:header});
  }

  activarUsuarioAdministrador(token:string,usuario_id:number)
  {
    let data={
      "usuario_id":usuario_id
    };
    let header : any = new Headers(),
    opsi   : any = JSON.stringify(data);
    header.append("Authorization",token);
    header.append("Content-Type","application/json");
    return this.http2.put(this.ServidorUrl+'usuario/admin/activar',opsi,{ headers:header});
  }
  eliminarUsuarioAdministrador(token:string,usuario_id:number)
  {
    let data={
      "usuario_id":usuario_id
    };
    let header : any = new Headers(),
    opsi   : any = JSON.stringify(data);
    header.append("Authorization",token);
    header.append("Content-Type","application/json");
    return this.http2.put(this.ServidorUrl+'usuario/admin/eliminar',opsi,{ headers:header});
  }

  registrarAdministrador(token:string,admin:any)
  {
    let data={
      "nombre":admin.nombre,
      "apellidos":admin.apellidos,
      "carnet":admin.carnet,
      "correo":admin.correo,
      "telefono":admin.telefono,
      "fecha_nacimiento":admin.fecha_nacimiento
    };
    let header : any = new Headers(),
    opsi   : any = JSON.stringify(data);
    header.append("Authorization",token);
    header.append("Content-Type","application/json");
    return this.http2.post(this.ServidorUrl+'registrar-administrador',opsi,{ headers:header});
  }

  obtenerInfoDeposito(token:string,carnet:number)
  {
    let data={
      "carnet":carnet
    };
    let header : any = new Headers(),
    opsi   : any = JSON.stringify(data);
    header.append("Authorization",token);
    header.append("Content-Type","application/json");
    return this.http2.post(this.ServidorUrl+'deposito/info/usuario',opsi,{ headers:header});
  }

  depositar(token,numero_cuenta,monto)
  {
    let data={
      "numero_cuenta":numero_cuenta,
      "monto":monto
    };
    let header : any = new Headers(),
    opsi   : any = JSON.stringify(data);
    header.append("Authorization",token);
    header.append("Content-Type","application/json");
    return this.http2.put(this.ServidorUrl+'depositar',opsi,{ headers:header});
  }

  obtenerInfoRetiro(token:string,carnet:number)
  {
    let data={
      "carnet":carnet
    };
    let header : any = new Headers(),
    opsi   : any = JSON.stringify(data);
    header.append("Authorization",token);
    header.append("Content-Type","application/json");
    return this.http2.post(this.ServidorUrl+'retiro/info/usuario',opsi,{ headers:header});
  }

  retirar(token,numero_cuenta)
  {
    let data={
      "numero_cuenta":numero_cuenta
    };
    let header : any = new Headers(),
    opsi   : any = JSON.stringify(data);
    header.append("Authorization",token);
    header.append("Content-Type","application/json");
    return this.http2.put(this.ServidorUrl+'retirar',opsi,{ headers:header});
  }

  obtenerCodigoResetPassword(token:string,carnet:number)
  {
    let data={
      "carnet":carnet
    };
    let header : any = new Headers(),
    opsi   : any = JSON.stringify(data);
    header.append("Authorization",token);
    header.append("Content-Type","application/json");
    return this.http2.post(this.ServidorUrl+'reset/password',opsi,{ headers:header});
  }

  verificarCodigo(token:string,carnet:number,codigo:string)
  {
    let data={
      "carnet":carnet,
      "codigo_reset_password":codigo
    };
    let header : any = new Headers(),
    opsi   : any = JSON.stringify(data);
    header.append("Authorization",token);
    header.append("Content-Type","application/json");
    return this.http2.post(this.ServidorUrl+'reset/password/verificar',opsi,{ headers:header});
  }
  cambiarPasswordReset(carnet:number,pass1:string,pass2:string)
  {
    let data={
      "password":pass1,
	    "repetir_password":pass2,
	    "carnet":carnet
    };
    let header : any = new Headers(),
    opsi   : any = JSON.stringify(data);  
    header.append("Content-Type","application/json");
    return this.http2.post(this.ServidorUrl+'reset/password/cambiar',opsi,{ headers:header});
  }

  cambiarPasswordDefault(token:string,pass1:string,)
  {
    let data={
      "password":pass1,
    };
    let header : any = new Headers(),
    opsi   : any = JSON.stringify(data); 
    header.append("Authorization",token); 
    header.append("Content-Type","application/json");
    return this.http2.post(this.ServidorUrl+'usuario/password/default',opsi,{ headers:header});
  }

  retiroCuentaEstudiante(token:string,usuario_id:number)
  {
    let data={
      "usuario_id":usuario_id,
    };
    let header : any = new Headers(),
    opsi   : any = JSON.stringify(data); 
    header.append("Authorization",token); 
    header.append("Content-Type","application/json");
    return this.http2.put(this.ServidorUrl+'usuario/retiro/fin/carrera',opsi,{ headers:header});
  }
  actualizarFotoPerfil(token:string,imagen:string,rol:string)
  {
    let data={
      "imagen":imagen,
      "rol":rol
    };
    let header : any = new Headers(),
    opsi   : any = JSON.stringify(data); 
    header.append("Authorization",token); 
    header.append("Content-Type","application/json");
    return this.http2.put(this.ServidorUrl+'usuario/foto',opsi,{ headers:header});
  }
  eliminarFotoPerfil(token:string)
  {
    let data={
      "imagen":'sin imagen'
    };
    let header : any = new Headers(),
    opsi   : any = JSON.stringify(data); 
    header.append("Authorization",token); 
    header.append("Content-Type","application/json");
    return this.http2.put(this.ServidorUrl+'usuario/foto/eliminar',opsi,{ headers:header});
  }

  obtenerCarreras(token:string)
  {
    let header : any = new Headers();
    header.append("Authorization",token);
    return this.http2.get(this.ServidorUrl+'carreras',{ headers:header});
  }

  obtenerGestiones(token:string)
  {
    let header : any = new Headers();
    header.append("Authorization",token);
    return this.http2.get(this.ServidorUrl+'gestiones',{ headers:header});
  }

  registrarGestion(token:string,gestion:any)
  {
    let data={
      "semestre":gestion.semestre,
      "ano":gestion.ano
      
    };
    let header : any = new Headers(),
    opsi   : any = JSON.stringify(data);
    header.append("Authorization",token);
    header.append("Content-Type","application/json");
    return this.http2.post(this.ServidorUrl+'gestion',opsi,{ headers:header});
  }

  activarGestion(token:string,gestion_id:number)
  {
    let data={
      "gestion_id":gestion_id
    };
    let header : any = new Headers(),
    opsi   : any = JSON.stringify(data);
    header.append("Authorization",token);
    header.append("Content-Type","application/json");
    return this.http2.put(this.ServidorUrl+'gestion/activar',opsi,{ headers:header});
  }

  eliminarGestion(token:string,gestion_id:number)
  {
    let data={
      "gestion_id":gestion_id
    };
    let header : any = new Headers(),
    opsi   : any = JSON.stringify(data);
    header.append("Authorization",token);
    header.append("Content-Type","application/json");
    return this.http2.put(this.ServidorUrl+'gestion/eliminar',opsi,{ headers:header});
  }

  registrarMensualidad(token:string,mensualidad:any)
  {
    let data={
      "nombre":mensualidad.nombre,
      "fecha_pago":mensualidad.fecha_pago,
      "monto":mensualidad.monto,
      "gestion_id":mensualidad.gestion_id
      
    };
    let header : any = new Headers(),
    opsi   : any = JSON.stringify(data);
    header.append("Authorization",token);
    header.append("Content-Type","application/json");
    return this.http2.post(this.ServidorUrl+'mensualidad',opsi,{ headers:header});
  }

  listarMensualidades(token:string,gestion_id:number)
  {
    let data={
      "gestion_id":gestion_id
    };
    let header : any = new Headers(),
    opsi   : any = JSON.stringify(data);
    header.append("Authorization",token);
    header.append("Content-Type","application/json");
    return this.http2.post(this.ServidorUrl+'mensualidades',opsi,{ headers:header});
  }

  editarMensualidad(token:string,mensualidad:any)
  {
    let data={
      "nombre":mensualidad.nombre,
      "fecha_pago":mensualidad.fecha_pago,
      "monto":mensualidad.monto,
      "mensualidad_id":mensualidad.mensualidad_id
      
    };
    let header : any = new Headers(),
    opsi   : any = JSON.stringify(data);
    header.append("Authorization",token);
    header.append("Content-Type","application/json");
    return this.http2.put(this.ServidorUrl+'mensualidad',opsi,{ headers:header});
  }

  obtenerSaldoUniversidad(token:string)
  {
    let header : any = new Headers();
    header.append("Authorization",token);
    return this.http2.get(this.ServidorUrl+'universidad/saldo',{ headers:header});
  }
  obtenerPagosMensualidad(token,gestion_id:number,mensualidad_id:number)
  {
    let data={
      "mensualidad_id":mensualidad_id,
      "gestion_id":gestion_id
    };
    let header : any = new Headers(),
    opsi   : any = JSON.stringify(data);
    header.append("Authorization",token);
    header.append("Content-Type","application/json");
    return this.http2.post(this.ServidorUrl+'mensualidades/pagos',opsi,{ headers:header});
  }

  obtenerPagosPorGestion(token,gestion_id:number)
  {
    let data={
      "gestion_id":gestion_id
    };
    let header : any = new Headers(),
    opsi   : any = JSON.stringify(data);
    header.append("Authorization",token);
    header.append("Content-Type","application/json");
    return this.http2.post(this.ServidorUrl+'gestiones/pagos',opsi,{ headers:header});
  }
}
