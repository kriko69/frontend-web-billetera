import { Injectable } from '@angular/core';

@Injectable()
export class NavbarService {

  visible: boolean;
  rol:string;
  constructor() { 
    this.visible = false;
    this.rol=sessionStorage.getItem('rol');
   }

  hide() { this.visible = false; }

  show() { this.visible = true; }

  toggle() { this.visible = !this.visible; }

}
