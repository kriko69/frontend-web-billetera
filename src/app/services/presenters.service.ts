import { Injectable } from '@angular/core';
import Swal from 'sweetalert2';

@Injectable()
export class PresentersService {

  constructor() { }

  presentAlertSuccess(mensaje:string)
  {
    Swal.fire({
      icon: 'success',
      title: 'Exito',
      text: mensaje,
    })
  }

  presentAlertFail(mensaje:string)
  {
    Swal.fire({
      icon: 'error',
      title: 'Error',
      text: mensaje,
    })
  }

  presentAlertInfo(titulo:string,mensaje:string)
  {
    Swal.fire({
      icon: 'info',
      title: titulo,
      text: mensaje,
    })
  }


}
