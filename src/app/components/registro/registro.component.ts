import { ParametersService } from './../../services/parameters.service';
import { Component, OnInit } from '@angular/core';
import { NavbarService } from 'app/services/navbar.service';
import { NgForm, FormControl, FormGroup,Validators } from '@angular/forms';
import { PresentersService } from 'app/services/presenters.service';
import { AdministradorServiceService } from 'app/services/administrador-service.service';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css']
})
export class RegistroComponent implements OnInit {

  dateMax;
  form:FormGroup;
  tipo:string;
  estudiante;
  administrador;
  tituloBoton="Registrar";
  mostrarActivar=false;
  seRegistra=true;
  tituloPage:string;
  carreras;
  cantidad_carreras;
  constructor(public nav:NavbarService,
    public parameters:ParametersService,
    public presenters:PresentersService,
    public aservice:AdministradorServiceService,
    public route:Router,
    public spinner:NgxSpinnerService) { 
    this.form= new FormGroup({
      'nombre': new FormControl('',[Validators.required]),
      'apellidos': new FormControl('',[Validators.required]),
      'carnet': new FormControl('',[Validators.required]),
      'correo': new FormControl('',[Validators.required, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$')]),
      'telefono': new FormControl('',[Validators.required]),
      'fecha_nacimiento': new FormControl('',[Validators.required]),
      'carrera': new FormControl('',[Validators.required])
    });
  }

  ngOnInit() {
    
    this.tipo=this.parameters.getTipoRegistroUsuario();
    console.log('tipo',this.tipo);
    this.verificarSiEditar();
    this.nav.show();
    this.obtenerFechaActual();
    this.obtenerCarreras();
  }

  registrar()
  {
    console.log(this.form.value);
    if(this.seRegistra)
    {
      console.log('se registra');
      this.registraEstudiante();
    }else{
      console.log(this.estudiante.nombre);
      console.log('se edita');
      
      this.estudiante.nombre=this.form.value.nombre;
      this.estudiante.apellidos=this.form.value.apellidos;
      this.estudiante.carnet=this.form.value.carnet;
      this.estudiante.correo=this.form.value.correo;
      this.estudiante.telefono=this.form.value.telefono;
      this.estudiante.fecha_nacimiento=this.form.value.fecha_nacimiento;
      this.estudiante.carrera=parseInt(this.form.value.carrera);
      console.log(this.estudiante);
      console.log(this.estudiante.carrera);
      
      let token=sessionStorage.getItem('token');
      let info:any;
      this.spinner.show();
      this.aservice.editarPerifl(token,this.estudiante).subscribe(
        (data)=>{
          this.spinner.hide();
          data=data.json();
          info=Object.assign(data);
          console.log('info',info);
          
          if(info.estado=="exito")
          {
            //exito
            this.presenters.presentAlertSuccess('Se actualizo al usuario correctamente');
          }else{
            //error
            //imprimir mensaje
            if(info.mensaje=="el token ha expirado."){
              //mandar a login
              this.presenters.presentAlertInfo('La sesion expiro','Por favor vuelva a ingresar.');
              this.parameters.setToken('');
              this.parameters.setRol('');
              sessionStorage.removeItem('token');
              sessionStorage.removeItem('rol');
              this.route.navigate(['/login']);
            }else{
              //imprimir mensaje
              this.presenters.presentAlertFail(info.mensaje);
            }
          }
        },(error)=>{
          this.spinner.hide();
          this.presenters.presentAlertFail('No hay conexion con el servidor, intente mas tarde.');
        }
      );
    }
    
  }
  actualizar(){
    console.log('se actualiza');
    console.log(this.estudiante);
    
  }

  obtenerFechaActual()
  {
    var hoy = new Date();
    var dd = hoy.getDate();
    var mm = hoy.getMonth()+1;
    var yyyy = hoy.getFullYear();
    let dia;
    let mes;
    if (dd < 10) {
      dia = '0' + dd;
    }else{
      dia=dd;
    }

    if (mm < 10) {
      mes = '0' + mm;
    }else{
      mes=mm;
    }
    this.dateMax=yyyy+'-'+mes+'-'+dia;
    console.log(this.dateMax);
    
  }

  verificarSiEditar()
  {
    this.estudiante=this.parameters.getEstudianteEditar();
    this.administrador=this.parameters.getAdministradorEditar();
    console.log('estudiante',this.estudiante);
    console.log('administrador',this.administrador);
    if(this.estudiante==null && this.administrador==null)
    {
      //se registra
      this.seRegistra=true;
      this.tituloPage='Registro de estudiante';
    }else{
      this.seRegistra=false;
      if(this.estudiante!=null)
      {
        //se edita estudiante
        this.tituloPage='Perfil del estudiante';
        this.parameters.setAdministradorEditar(null);
        this.parameters.setEstudianteEditar(null);
        this.tituloBoton="Actualizar estudiante";
        this.form.patchValue({
          'nombre': this.estudiante.nombre,
          'apellidos': this.estudiante.apellidos,
          'carnet': this.estudiante.carnet,
          'correo': this.estudiante.correo,
          'telefono': this.estudiante.telefono,
          'fecha_nacimiento': this.estudiante.fecha_nacimiento,
          'carrera': this.estudiante.carrera
        });
        
        if(this.estudiante.estado==1)
        {
          this.mostrarActivar=true;
        }
      } else{
        if(this.administrador!=null){
          //se edita administrador
          this.tituloPage='Perfil del administrador';
          this.parameters.setAdministradorEditar(null);
          this.parameters.setEstudianteEditar(null);
          this.tituloBoton="Actualizar Administrador";
          this.form.patchValue({
            'nombre': this.administrador.nombre,
            'apellidos': this.administrador.apellidos,
            'carnet': this.administrador.carnet,
            'correo': this.administrador.correo,
            'telefono': this.administrador.telefono,
            'fecha_nacimiento': this.administrador.fecha_nacimiento,
            'carrera': ""
          });
          this.estudiante=this.administrador; 
        }
      }
    }
  }

  activarUsuario()
  {
    console.log(this.estudiante.usuario_id);
    let usuario_id=this.estudiante.usuario_id;
    let token=sessionStorage.getItem('token');
    let info:any;
    this.spinner.show();
    this.aservice.activarUsuario(token,usuario_id).subscribe(
      (data)=>{
        this.spinner.hide();
        data=data.json();
        info=Object.assign(data);
        console.log('info',info);
        
        if(info.estado=="exito")
        {
          //exito
          this.presenters.presentAlertSuccess('Se activo al usuario correctamente');
        }else{
          //error
          //imprimir mensaje
          if(info.mensaje=="el token ha expirado."){
            //mandar a login
            this.presenters.presentAlertInfo('La sesion expiro','Por favor vuelva a ingresar.');
            this.parameters.setToken('');
            this.parameters.setRol('');
            sessionStorage.removeItem('token');
            sessionStorage.removeItem('rol');
            this.route.navigate(['/login']);
          }else{
            //imprimir mensaje
            this.presenters.presentAlertFail(info.mensaje);
          }
        }
      },(error)=>{
        this.spinner.hide();
        this.presenters.presentAlertFail('No hay conexion con el servidor, intente mas tarde.');
      }
    );
    
  }

  registraEstudiante()
  {
    let info:any;
    this.spinner.show();
    let usuario_id;
    let password_default;
    let token=sessionStorage.getItem('token');
    this.aservice.registrarEstudiante(token,this.form.value).subscribe(
      (data)=>{
        this.spinner.hide();
        data=data.json(); //parse to json
        info=Object.assign(data);
        console.log('info',info);
        
        if(info.estado=="exito")
        {
          //exito
          usuario_id=info.usuario_id;
          password_default=info.password_default;
          console.log('usuario_id',usuario_id);
          console.log('password_default',password_default);
          this.registroCuenta(usuario_id,password_default);
          
        }else{
          //error
          if(info.mensaje=="el token ha expirado."){
            //mandar a login
            this.presenters.presentAlertInfo('La sesion expiro','Por favor vuelva a ingresar.');
            this.parameters.setToken('');
            this.parameters.setRol('');
            sessionStorage.removeItem('token');
            sessionStorage.removeItem('rol');
            this.route.navigate(['/login']);
          }else{
            //imprimir mensaje
            this.presenters.presentAlertFail(info.mensaje);
          }
        }

      },(error)=>{
        //error en la conexion
        this.spinner.hide();
        this.presenters.presentAlertFail('No hay conexion con el servidor, intente mas tarde.');
      }
    );
  }

  registroCuenta(usuario_id,password_default)
  {
    let info:any;
    this.spinner.show();
    let token=sessionStorage.getItem('token');
    this.aservice.registrarCuentaEstudiante(token,usuario_id,password_default).subscribe(
      (data)=>{
        this.spinner.hide();
        data=data.json(); //parse to json
        info=Object.assign(data);
        console.log('info',info);
        
        if(info.estado=="exito")
        {
          //exito
          this.presenters.presentAlertSuccess('Se creo usuario y cuenta correctamente.');
          this.route.navigate(['/usuarios']);
        }else{
          //error
          if(info.mensaje=="el token ha expirado."){
            //mandar a login
            this.presenters.presentAlertInfo('La sesion expiro','Por favor vuelva a ingresar.');
            this.parameters.setToken('');
            this.parameters.setRol('');
            sessionStorage.removeItem('token');
            sessionStorage.removeItem('rol');
            this.route.navigate(['/login']);
          }else{
            //imprimir mensaje
            this.presenters.presentAlertFail(info.mensaje);
          }
        }

      },(error)=>{
        //error en la conexion
        this.spinner.hide();
        this.presenters.presentAlertFail('No hay conexion con el servidor, intente mas tarde.');
      }
    );
  }

  obtenerCarreras()
  {
    let info:any;
    this.spinner.show();
    let token=sessionStorage.getItem('token');
    this.aservice.obtenerCarreras(token).subscribe(
      (data)=>{
        this.spinner.hide();
        data=data.json(); //parse to json
        info=Object.assign(data);
        console.log('info',info);
        if(info.estado=="exito")
        {
          //exito
          if(info.data==null)
          {
            this.carreras=[];
            this.cantidad_carreras=0
          }else{
            this.carreras=info.data;
            this.cantidad_carreras=this.carreras.length;
            console.log('carreras:',this.carreras);
            
          }
        }else{
          //error
          if(info.mensaje=="el token ha expirado."){
            //mandar a login
            this.presenters.presentAlertInfo('La sesion expiro','Por favor vuelva a ingresar.');
            this.parameters.setToken('');
            this.parameters.setRol('');
            sessionStorage.removeItem('token');
            sessionStorage.removeItem('rol');
            this.route.navigate(['/login']);
          }else{
            //imprimir mensaje
            this.presenters.presentAlertFail(info.mensaje);
          }
        }
      },(error)=>{
        //error en la conexion
        this.spinner.hide();
        this.presenters.presentAlertFail('No hay conexion con el servidor, intente mas tarde.');
      }
    );
  }

}
