import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PagoGestionesComponent } from './pago-gestiones.component';

describe('PagoGestionesComponent', () => {
  let component: PagoGestionesComponent;
  let fixture: ComponentFixture<PagoGestionesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PagoGestionesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PagoGestionesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
