import { ParametersService } from './../../services/parameters.service';
import { Component, OnInit } from '@angular/core';
import { NavbarService } from 'app/services/navbar.service';
import { NgForm, FormControl, FormGroup,Validators, Form } from '@angular/forms';
import { PresentersService } from 'app/services/presenters.service';
import { AdministradorServiceService } from 'app/services/administrador-service.service';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-agregar-mensualidad',
  templateUrl: './agregar-mensualidad.component.html',
  styleUrls: ['./agregar-mensualidad.component.css']
})
export class AgregarMensualidadComponent implements OnInit {

  form:FormGroup;
  gestion_padre;
  mensualidad;
  constructor(public nav:NavbarService,
    public parameters:ParametersService,
    public presenters:PresentersService,
    public aservice:AdministradorServiceService,
    public route:Router,
    public spinner:NgxSpinnerService) { 
    this.form= new FormGroup({
      'nombre': new FormControl('',[Validators.required]),
      'fecha_pago': new FormControl('',[Validators.required]),
      'monto': new FormControl('',[Validators.required]),
      'gestion_id': new FormControl()
    });
  }

  ngOnInit() {
    this.nav.show();
    this.gestion_padre=this.parameters.getGestion();
  }

  atras()
  {
    this.route.navigate(['/gestiones']);
  }


  registrar()
  {
    console.log(this.form.value);
    let fecha=this.form.value.fecha_pago;
    console.log(fecha);
    let resFecha = fecha.split("-");
    let reversedFecha = resFecha.reverse();
    fecha=reversedFecha[0]+'-'+reversedFecha[1]+'-'+reversedFecha[2];
    console.log(fecha);
    this.form.value.fecha_pago=fecha;
    this.form.value.gestion_id=this.gestion_padre.gestion_id;
    this.mensualidad=Object.assign(this.form.value);
    console.log(this.mensualidad);
      let token=sessionStorage.getItem('token');
      let info:any;
      this.spinner.show();
      this.aservice.registrarMensualidad(token,this.mensualidad).subscribe(
        (data)=>{
          this.spinner.hide();
          data=data.json();
          info=Object.assign(data);
          console.log('info',info);
          
          if(info.estado=="exito")
          {
            //exito
            this.presenters.presentAlertSuccess('Se registro la mensualidad correctamente');
            setTimeout(()=>{
              this.route.navigate(['/gestiones']);
            },1500);
          }else{
            //error
            //imprimir mensaje
            if(info.mensaje=="el token ha expirado."){
              //mandar a login
              this.presenters.presentAlertInfo('La sesion expiro','Por favor vuelva a ingresar.');
              this.parameters.setToken('');
              this.parameters.setRol('');
              sessionStorage.removeItem('token');
              sessionStorage.removeItem('rol');
              this.route.navigate(['/login']);
            }else{
              //imprimir mensaje
              this.presenters.presentAlertFail(info.mensaje);
            }
          }
        },(error)=>{
          this.spinner.hide();
          this.presenters.presentAlertFail('No hay conexion con el servidor, intente mas tarde.');
        }
      );
  }
  

  

}
