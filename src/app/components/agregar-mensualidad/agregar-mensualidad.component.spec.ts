import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgregarMensualidadComponent } from './agregar-mensualidad.component';

describe('AgregarMensualidadComponent', () => {
  let component: AgregarMensualidadComponent;
  let fixture: ComponentFixture<AgregarMensualidadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgregarMensualidadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgregarMensualidadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
