import { NavbarService } from 'app/services/navbar.service';
import { Component, OnInit } from '@angular/core';
import { NgForm, FormControl, FormGroup,Validators } from '@angular/forms';
import { AdministradorServiceService } from 'app/services/administrador-service.service';
import { PresentersService } from 'app/services/presenters.service';
import { ParametersService } from 'app/services/parameters.service';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { NgxSpinnerService } from 'ngx-spinner';
@Component({
  selector: 'app-deposito',
  templateUrl: './deposito.component.html',
  styleUrls: ['./deposito.component.css']
})
export class DepositoComponent implements OnInit {

  mostrar=false;
  mostrarSpinner=false;
  carnet:number;
  monto:number;
  form:FormGroup;
  dataUsuario:any;
  constructor(public nav:NavbarService,
    public aservice:AdministradorServiceService,
    public presenters:PresentersService,
    public parameters:ParametersService,
    public route:Router,
    public spinner:NgxSpinnerService) {
    this.form= new FormGroup({
      'carnet': new FormControl(''),
      'monto': new FormControl('',[Validators.required])
    });
   }

  ngOnInit() {
    this.nav.show();  
  }

  buscar()
  {
    if(this.carnet==undefined || this.carnet==0)
    {
      this.presenters.presentAlertFail('Carnet invalido.');
    }else{
      this.mostrarSpinner=true;
      this.obtenerInfoDeposito();
      
      
    }
    
  }

  depositar()
  {
    console.log('datos deposito: ',this.dataUsuario.cuentas[0].numero_cuenta);
    console.log('datos deposito: ',this.monto);
    let cuenta=this.dataUsuario.cuentas[0].numero_cuenta;
    let monto=this.monto;
    Swal.fire({
      title: 'Esta seguro de realizar el deposito?',
      text: "Una vez depositado no se puede retirar el dinero.",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, hazlo'
    }).then((result) => {
      if (result.value) {
        this.realizarDeposito(cuenta,monto);
        
      }
    })
    
  }

  obtenerInfoDeposito()
  {
    let info:any;
    this.spinner.show();
    let token=sessionStorage.getItem('token');
    this.aservice.obtenerInfoDeposito(token,this.carnet).subscribe(
      (data)=>{
        this.spinner.hide();
        data=data.json(); //parse to json
        info=Object.assign(data);
        console.log('info',info);
        
        if(info.estado=="exito")
        {
          //exito
          this.mostrarSpinner=false;
          this.mostrar=true;
          this.dataUsuario=info.data;
        }else{
          //error
          if(info.mensaje=="el token ha expirado."){
            //mandar a login
            this.mostrarSpinner=false;
            this.presenters.presentAlertInfo('La sesion expiro','Por favor vuelva a ingresar.');
            this.parameters.setToken('');
            this.parameters.setRol('');
            sessionStorage.removeItem('token');
            sessionStorage.removeItem('rol');
            this.route.navigate(['/login']);
          }else{
            //imprimir mensaje
            this.mostrarSpinner=false;
            this.presenters.presentAlertFail(info.mensaje);
          }
        }

      },(error)=>{
        //error en la conexion
        this.spinner.hide();
        this.mostrarSpinner=false;
        this.presenters.presentAlertFail('No hay conexion con el servidor, intente mas tarde.');
      }
    );
  }

  realizarDeposito(cuenta:number,monto:number)
  {
    let info:any;
    this.spinner.show();
    let token=sessionStorage.getItem('token');
    this.aservice.depositar(token,cuenta,monto).subscribe(
      (data)=>{
        this.spinner.hide();
        data=data.json(); //parse to json
        info=Object.assign(data);
        console.log('info',info);
        
        if(info.estado=="exito")
        {
          //exito
          this.presenters.presentAlertSuccess('Se realizo el deposito correctamente.');
        }else{
          //error
          if(info.mensaje=="el token ha expirado."){
            //mandar a login
            this.presenters.presentAlertInfo('La sesion expiro','Por favor vuelva a ingresar.');
            this.parameters.setToken('');
            this.parameters.setRol('');
            sessionStorage.removeItem('token');
            sessionStorage.removeItem('rol');
            this.route.navigate(['/login']);
          }else{
            //imprimir mensaje
            this.presenters.presentAlertFail(info.mensaje);
          }
        }

      },(error)=>{
        //error en la conexion
        this.spinner.hide();
        this.presenters.presentAlertFail('No hay conexion con el servidor, intente mas tarde.');
      }
    );
  }

}
