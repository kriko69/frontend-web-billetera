import { Router } from '@angular/router';
import { NavbarService } from 'app/services/navbar.service';
import { Component, OnInit } from '@angular/core';
import { AdministradorServiceService } from 'app/services/administrador-service.service';
import { ParametersService } from 'app/services/parameters.service';
import { PresentersService } from 'app/services/presenters.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-afiliacion',
  templateUrl: './afiliacion.component.html',
  styleUrls: ['./afiliacion.component.css']
})
export class AfiliacionComponent implements OnInit {

  negocios: any;
  cantidad: number;
  copia_negocios_original;
  nombre_negocio;
  page:number=1;
  constructor(public nav: NavbarService,
    public route: Router,
    public aservice: AdministradorServiceService,
    public parameters: ParametersService,
    public presenters: PresentersService,
    public spinner:NgxSpinnerService) {

  }

  ngOnInit() {
    this.nav.show();
    this.obtenerNegocios();
  }

  obtenerNegocios() {
    let info: any;
    this.spinner.show();
    let token = sessionStorage.getItem('token');
    this.aservice.listarNegocios(token).subscribe(
      (data) => {
        this.spinner.hide();
        data = data.json(); //parse to json
        info = Object.assign(data);
        console.log('info', info);

        if (info.estado == "exito") {
          //exito
          if (info.data != null) {
            this.negocios = info.data;
            this.copia_negocios_original = this.negocios;
            this.cantidad = this.negocios.length;
          } else {
            this.negocios = [];
            this.cantidad = 0;
          }

        } else {
          //error
          if (info.mensaje == "el token ha expirado.") {
            //mandar a login
            this.presenters.presentAlertInfo('La sesion expiro', 'Por favor vuelva a ingresar.');
            this.parameters.setToken('');
            this.parameters.setRol('');
            sessionStorage.removeItem('token');
            sessionStorage.removeItem('rol');
            this.route.navigate(['/login']);
          } else {
            //imprimir mensaje
            this.presenters.presentAlertFail(info.mensaje);
          }
        }

      }, (error) => {
        //error en la conexion
        this.spinner.hide();
        this.presenters.presentAlertFail('No hay conexion con el servidor, intente mas tarde.');
      }
    );
  }

  aceptarAfiliacion(negocio_id) {
    console.log(negocio_id);
    let info:any;
    this.spinner.show();
    let token=sessionStorage.getItem('token');
    this.aservice.aceptarAfiliacionNegocio(token,negocio_id).subscribe(
      (data)=>{
        this.spinner.hide();
        data=data.json(); //parse to json
        info=Object.assign(data);
        console.log('info',info);
        
        if(info.estado=="exito")
        {
          //exito
          this.presenters.presentAlertSuccess('Negocio afiliado correctamente.');
          setTimeout(()=>{
            window.location.reload();
          },1000);
          
        }else{
          //error
          if(info.mensaje=="el token ha expirado."){
            //mandar a login
            this.presenters.presentAlertInfo('La sesion expiro','Por favor vuelva a ingresar.');
            this.parameters.setToken('');
            this.parameters.setRol('');
            sessionStorage.removeItem('token');
            sessionStorage.removeItem('rol');
            this.route.navigate(['/login']);
          }else{
            //imprimir mensaje
            this.presenters.presentAlertFail(info.mensaje);
          }
        }

      },(error)=>{
        //error en la conexion
        this.spinner.hide();
        this.presenters.presentAlertFail('No hay conexion con el servidor, intente mas tarde.');
      }
    );
  }

  rechazarAfiliacion(negocio_id,usuario_id) {
    console.log(negocio_id);
    console.log(usuario_id);
    let info:any;
    this.spinner.show();
    let token=sessionStorage.getItem('token');
    this.aservice.rechazarAfiliacionNegocio(token,negocio_id,usuario_id).subscribe(
      (data)=>{
        this.spinner.hide();
        data=data.json(); //parse to json
        info=Object.assign(data);
        console.log('info',info);
        
        if(info.estado=="exito")
        {
          //exito
          this.presenters.presentAlertSuccess('Negocio rechazado correctamente.');
          setTimeout(()=>{
            window.location.reload();
          },1000);
          
        }else{
          //error
          if(info.mensaje=="el token ha expirado."){
            //mandar a login
            this.presenters.presentAlertInfo('La sesion expiro','Por favor vuelva a ingresar.');
            this.parameters.setToken('');
            this.parameters.setRol('');
            sessionStorage.removeItem('token');
            sessionStorage.removeItem('rol');
            this.route.navigate(['/login']);
          }else{
            //imprimir mensaje
            this.presenters.presentAlertFail(info.mensaje);
          }
        }

      },(error)=>{
        //error en la conexion
        this.spinner.hide();
        this.presenters.presentAlertFail('No hay conexion con el servidor, intente mas tarde.');
      }
    );

  }

  eliminarNegocio(negocio_id) {
    console.log(negocio_id);
    let info:any;
    this.spinner.show();
    let token=sessionStorage.getItem('token');
    this.aservice.eliminarNegocio(token,negocio_id).subscribe(
      (data)=>{
        this.spinner.hide();
        data=data.json(); //parse to json
        info=Object.assign(data);
        console.log('info',info);
        
        if(info.estado=="exito")
        {
          //exito
          this.presenters.presentAlertSuccess('Negocio eliminado con exito.');
          setTimeout(()=>{
            window.location.reload();
          },1000);
          
        }else{
          //error
          if(info.mensaje=="el token ha expirado."){
            //mandar a login
            this.presenters.presentAlertInfo('La sesion expiro','Por favor vuelva a ingresar.');
            this.parameters.setToken('');
            this.parameters.setRol('');
            sessionStorage.removeItem('token');
            sessionStorage.removeItem('rol');
            this.route.navigate(['/login']);
          }else{
            //imprimir mensaje
            this.presenters.presentAlertFail(info.mensaje);
          }
        }

      },(error)=>{
        //error en la conexion
        this.spinner.hide();
        this.presenters.presentAlertFail('No hay conexion con el servidor, intente mas tarde.');
      }
    );

  }

  activarNegocio(negocio_id) {
    console.log(negocio_id);
    let info:any;
    this.spinner.show();
    let token=sessionStorage.getItem('token');
    this.aservice.activarNegocio(token,negocio_id).subscribe(
      (data)=>{
        this.spinner.hide();
        data=data.json(); //parse to json
        info=Object.assign(data);
        console.log('info',info);
        
        if(info.estado=="exito")
        {
          //exito
          this.presenters.presentAlertSuccess('Negocio activado con exito.');
          setTimeout(()=>{
            window.location.reload();
          },1000);
          
        }else{
          //error
          if(info.mensaje=="el token ha expirado."){
            //mandar a login
            this.presenters.presentAlertInfo('La sesion expiro','Por favor vuelva a ingresar.');
            this.parameters.setToken('');
            this.parameters.setRol('');
            sessionStorage.removeItem('token');
            sessionStorage.removeItem('rol');
            this.route.navigate(['/login']);
          }else{
            //imprimir mensaje
            this.presenters.presentAlertFail(info.mensaje);
          }
        }

      },(error)=>{
        //error en la conexion
        this.spinner.hide();
        this.presenters.presentAlertFail('No hay conexion con el servidor, intente mas tarde.');
      }
    );

  }

  copia_negocios:any;
  resultados:any=[];
  buscar()
  {
    console.log(this.nombre_negocio);
    if(this.nombre_negocio==null){
      console.log(this.copia_negocios);
      
      this.negocios=this.copia_negocios_original;
      this.cantidad=this.copia_negocios_original.length;
    }else{
      let nombre1=this.nombre_negocio.toUpperCase();
      setTimeout(()=>{
        this.copia_negocios=this.copia_negocios_original;
        console.log(this.copia_negocios);
        
        for (let i = 0; i < this.copia_negocios.length; i++) {
          let nombre2=this.copia_negocios[i].nombre_negocio;
          if(nombre2.toUpperCase().search(nombre1)>-1)
          {
            this.resultados.push(this.copia_negocios[i]);
          } 
        }
        
        this.cantidad=this.resultados.length;
        this.negocios=this.resultados;
        console.log(this.negocios);
        this.resultados=[];
      },1000);
    }
  }

}
