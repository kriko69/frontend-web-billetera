import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PagosMensualidadComponent } from './pagos-mensualidad.component';

describe('PagosMensualidadComponent', () => {
  let component: PagosMensualidadComponent;
  let fixture: ComponentFixture<PagosMensualidadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PagosMensualidadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PagosMensualidadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
