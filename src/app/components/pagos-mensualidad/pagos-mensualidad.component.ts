import { NavbarService } from 'app/services/navbar.service';
import { Component, OnInit } from '@angular/core';
import { AdministradorServiceService } from 'app/services/administrador-service.service';
import { ParametersService } from 'app/services/parameters.service';
import { PresentersService } from 'app/services/presenters.service';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import  Swal  from 'sweetalert2';
import { isNullOrUndefined } from 'util';

@Component({
  selector: 'app-pagos-mensualidad',
  templateUrl: './pagos-mensualidad.component.html',
  styleUrls: ['./pagos-mensualidad.component.css']
})
export class PagosMensualidadComponent implements OnInit {

  page:number=1;
  gestion_padre;
  mensualidad_padre;
  pagos;
  cantidad;
  copia_pagos_original;
  carnet_estudiante
  cuenta_universidad;
  total;
  total_esta_mensualidad=0;
  constructor(
    public navbar:NavbarService,
    public route:Router,
    public aservice:AdministradorServiceService,
    public parameters:ParametersService,
    public presenters:PresentersService,
    public spinner:NgxSpinnerService
    ) {

   }

  ngOnInit() {
    this.navbar.show();
    this.gestion_padre=this.parameters.getGestion();
    console.log(this.gestion_padre);
    
    this.mensualidad_padre=this.parameters.getMensualidad();
    console.log(this.mensualidad_padre);
    
    if(isNullOrUndefined(this.gestion_padre))
    {
      this.route.navigate(['/gestiones']);
    }
    this.obtenerTotal();
    this.obtenerPagos();
  }

  atras()
  {
    this.route.navigate(['/ver-mensualidad']);
  }

  obtenerPagos()
  {
    let info:any;
    this.spinner.show();
    let token=sessionStorage.getItem('token');
    this.aservice.obtenerPagosMensualidad(token,this.gestion_padre.gestion_id,this.mensualidad_padre.mensualidad_id).subscribe(
      (data)=>{
        this.spinner.hide();
        data=data.json(); //parse to json
        info=Object.assign(data);
        console.log('info mensualidades',info);
        
        if(info.estado=="exito")
        {
          //exito
          if(info.data!=null)
          {
            this.pagos=info.data;
            this.copia_pagos_original=this.pagos;
            this.cantidad=this.pagos.length;
            this.sacarTotal();
          }else{
            this.pagos=[];
            this.cantidad=0;
          }
          
        }else{
          //error
          if(info.mensaje=="el token ha expirado."){
            //mandar a login
            this.presenters.presentAlertInfo('La sesion expiro','Por favor vuelva a ingresar.');
            this.parameters.setToken('');
            this.parameters.setRol('');
            sessionStorage.removeItem('token');
            sessionStorage.removeItem('rol');
            this.route.navigate(['/login']);
          }else{
            //imprimir mensaje
            this.presenters.presentAlertFail(info.mensaje);
          }
        }

      },(error)=>{
        //error en la conexion
        this.spinner.hide();
        this.presenters.presentAlertFail('No hay conexion con el servidor, intente mas tarde.');
      }
    );
  }

  copia_pagos:any;
  resultados:any=[];
  buscar()
  {
    if(this.carnet_estudiante==null){
      
      this.pagos=this.copia_pagos_original;
      this.cantidad=this.copia_pagos_original.length;
    }else{
      setTimeout(()=>{
        this.copia_pagos=this.copia_pagos_original;
        console.log(this.copia_pagos);
        
        for (let i = 0; i < this.copia_pagos.length; i++) {
          let carnet=this.copia_pagos[i].carnet;
          if(carnet==this.carnet_estudiante)
          {
            this.resultados.push(this.copia_pagos[i]);
          } 
        }
        
        this.cantidad=this.resultados.length;
        this.pagos=this.resultados;
        console.log(this.pagos);
        this.resultados=[];
      },1000);
    }
  }

  obtenerTotal()
  {
    let info:any;
    this.spinner.show();
    let token=sessionStorage.getItem('token');
    this.aservice.obtenerSaldoUniversidad(token).subscribe(
      (data)=>{
        this.spinner.hide();
        data=data.json(); //parse to json
        info=Object.assign(data);
        console.log('info',info);
        
        if(info.estado=="exito")
        {
          //exito
          this.cuenta_universidad=info.data;
          this.total=this.cuenta_universidad.saldo;
        }else{
          //error
          if(info.mensaje=="el token ha expirado."){
            //mandar a login
            this.presenters.presentAlertInfo('La sesion expiro','Por favor vuelva a ingresar.');
            this.parameters.setToken('');
            this.parameters.setRol('');
            sessionStorage.removeItem('token');
            sessionStorage.removeItem('rol');
            this.route.navigate(['/login']);
          }else{
            //imprimir mensaje
            this.presenters.presentAlertFail(info.mensaje);
          }
        }

      },(error)=>{
        //error en la conexion
        this.spinner.hide();
        this.presenters.presentAlertFail('No hay conexion con el servidor, intente mas tarde.');
      }
    );
  }

  sacarTotal()
  {
    let montos=0;
    let multas=0;
    for (let i = 0; i < this.pagos.length; i++) {
      montos=montos+this.pagos[i].monto;
      multas=multas+this.pagos[i].multa;
    }
    this.total_esta_mensualidad=montos+multas;
  }

}
