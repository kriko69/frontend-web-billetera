import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { NavbarService } from '../../services/navbar.service';
import { ParametersService } from 'app/services/parameters.service';
import { AdministradorServiceService } from 'app/services/administrador-service.service';
import { PresentersService } from 'app/services/presenters.service';
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'app-admins',
  templateUrl: './admins.component.html',
  styleUrls: ['./admins.component.css']
})
export class AdminsComponent implements OnInit {

  
  usuarios:any;
  cantidad:number;
  copia_usuarios_original:any;
  carnet:number;
  page:number=1;
  constructor(public nav:NavbarService,
    public route:Router,
    public parameters:ParametersService,
    public aservice:AdministradorServiceService,
    public presenters:PresentersService,
    public spinner:NgxSpinnerService) { 

  }

  ngOnInit() {
    this.nav.show();
    this.obtenerUsuariosAdministradores();
  }

  irRegistro()
  {
    this.parameters.setTipoRegistroUsuario("administrador");
    this.route.navigate(['/registro-admin']);
  }

  editar(usuario)
  {
    this.parameters.setTipoRegistroUsuario("administrador");
    this.parameters.setAdministradorEditar(usuario);
    this.route.navigate(['/registro']);
  }

  obtenerUsuariosAdministradores()
  {
    let info:any;
    let token=sessionStorage.getItem('token');
    this.spinner.show();
    this.aservice.obtenerListaUsuariosAdministradores(token).subscribe(
      (data)=>{
        this.spinner.hide();
        data=data.json(); //parse to json
        info=Object.assign(data);
        console.log('info',info);
        if(info.estado=="exito")
        {
          //exito
          if(info.data==null)
          {
            this.usuarios=[];
            this.cantidad=0
          }else{
            this.usuarios=info.data;
            this.copia_usuarios_original=this.usuarios;
            this.cantidad=this.usuarios.length;
            console.log('usuarios:',this.usuarios);
            
          }
        }else{
          //error
          if(info.mensaje=="el token ha expirado."){
            //mandar a login
            this.presenters.presentAlertInfo('La sesion expiro','Por favor vuelva a ingresar.');
            this.parameters.setToken('');
            this.parameters.setRol('');
            sessionStorage.removeItem('token');
            sessionStorage.removeItem('rol');
            this.route.navigate(['/login']);
          }else{
            //imprimir mensaje
            this.presenters.presentAlertFail(info.mensaje);
          }
        }
      },(error)=>{
        //error en la conexion
        this.spinner.hide();
        this.presenters.presentAlertFail('No hay conexion con el servidor, intente mas tarde.');
      }
    );
  }

  copia_usuarios:any;
  resultados:any=[];
  buscar()
  {
    console.log(this.carnet);
    if(this.carnet==null){
      console.log(this.copia_usuarios);
      
      this.usuarios=this.copia_usuarios_original;
      this.cantidad=this.copia_usuarios_original.length;
    }else{
      setTimeout(()=>{
        this.copia_usuarios=this.copia_usuarios_original;
        console.log(this.copia_usuarios);
        
        for (let i = 0; i < this.copia_usuarios.length; i++) {
          let carn=this.copia_usuarios[i].carnet;
          if(carn==this.carnet)
          {
            this.resultados.push(this.copia_usuarios[i]);
          } 
        }
        
        this.cantidad=this.resultados.length;
        this.usuarios=this.resultados;
        console.log(this.usuarios);
        this.resultados=[];
      },1000);
    }
    
  }

  activar(usuario)
  {
    let info:any;
    this.spinner.show();
    let id=usuario.usuario_id;
    let token=sessionStorage.getItem('token');
    this.aservice.activarUsuarioAdministrador(token,id).subscribe(
      (data)=>{
        this.spinner.hide();
        data=data.json(); //parse to json
        info=Object.assign(data);
        console.log('info',info);
        if(info.estado=="exito")
        {
          //exito
          this.presenters.presentAlertSuccess('Se activo el administrador correctamente.');
          setTimeout(()=>{
            window.location.reload();
          },1000);
        }else{
          //error
          if(info.mensaje=="el token ha expirado."){
            //mandar a login
            this.presenters.presentAlertInfo('La sesion expiro','Por favor vuelva a ingresar.');
            this.parameters.setToken('');
            this.parameters.setRol('');
            sessionStorage.removeItem('token');
            sessionStorage.removeItem('rol');
            this.route.navigate(['/login']);
          }else{
            //imprimir mensaje
            this.presenters.presentAlertFail(info.mensaje);
          }
        }
      },(error)=>{
        //error en la conexion
        this.spinner.hide();
        this.presenters.presentAlertFail('No hay conexion con el servidor, intente mas tarde.');
      }
    );
  }

  eliminar(usuario)
  {
    let info:any;
    this.spinner.show();
    let id=usuario.usuario_id;
    let token=sessionStorage.getItem('token');
    this.aservice.eliminarUsuarioAdministrador(token,id).subscribe(
      (data)=>{
        this.spinner.hide();
        data=data.json(); //parse to json
        info=Object.assign(data);
        console.log('info',info);
        if(info.estado=="exito")
        {
          //exito
          this.presenters.presentAlertSuccess('Se elimino el administrador correctamente.');
          setTimeout(()=>{
            window.location.reload();
          },1000);
        }else{
          //error
          if(info.mensaje=="el token ha expirado."){
            //mandar a login
            this.presenters.presentAlertInfo('La sesion expiro','Por favor vuelva a ingresar.');
            this.parameters.setToken('');
            this.parameters.setRol('');
            sessionStorage.removeItem('token');
            sessionStorage.removeItem('rol');
            this.route.navigate(['/login']);
          }else{
            //imprimir mensaje
            this.presenters.presentAlertFail(info.mensaje);
          }
        }
      },(error)=>{
        //error en la conexion
        this.spinner.hide();
        this.presenters.presentAlertFail('No hay conexion con el servidor, intente mas tarde.');
      }
    );
  }

}
