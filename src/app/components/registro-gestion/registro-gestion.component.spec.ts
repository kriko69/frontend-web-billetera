import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistroGestionComponent } from './registro-gestion.component';

describe('RegistroGestionComponent', () => {
  let component: RegistroGestionComponent;
  let fixture: ComponentFixture<RegistroGestionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegistroGestionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistroGestionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
