import { ParametersService } from './../../services/parameters.service';
import { Component, OnInit } from '@angular/core';
import { NavbarService } from 'app/services/navbar.service';
import { NgForm, FormControl, FormGroup,Validators } from '@angular/forms';
import { PresentersService } from 'app/services/presenters.service';
import { AdministradorServiceService } from 'app/services/administrador-service.service';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-registro-gestion',
  templateUrl: './registro-gestion.component.html',
  styleUrls: ['./registro-gestion.component.css']
})
export class RegistroGestionComponent implements OnInit {

  form:FormGroup;
  gestion;
  anos=[];
  ano_actual;
  constructor(public nav:NavbarService,
    public parameters:ParametersService,
    public presenters:PresentersService,
    public aservice:AdministradorServiceService,
    public route:Router,
    public spinner:NgxSpinnerService) { 
    this.form= new FormGroup({
      'semestre': new FormControl('',[Validators.required]),
      'ano': new FormControl('',[Validators.required]),
      
    });
  }
  ngOnInit() {
    this.nav.show();
    this.obtenerFechaActual();
  }

  obtenerFechaActual()
  {
    var hoy = new Date();
    var yyyy = hoy.getFullYear();
    this.ano_actual=yyyy;    
    for (let i = 0; i < 10; i++) {
      let aux= yyyy+i;
      this.anos.push(aux);
    }
    console.log(this.anos);
    
    
  }


  registrarGestion()
  {
    
    this.gestion=Object.assign(this.form.value);
    console.log(this.gestion);
      let token=sessionStorage.getItem('token');
      let info:any;
      this.spinner.show();
      this.aservice.registrarGestion(token,this.gestion).subscribe(
        (data)=>{
          this.spinner.hide();
          data=data.json();
          info=Object.assign(data);
          console.log('info',info);
          
          if(info.estado=="exito")
          {
            //exito
            this.presenters.presentAlertSuccess('Se registro la gestion correctamente');
          }else{
            //error
            //imprimir mensaje
            if(info.mensaje=="el token ha expirado."){
              //mandar a login
              this.presenters.presentAlertInfo('La sesion expiro','Por favor vuelva a ingresar.');
              this.parameters.setToken('');
              this.parameters.setRol('');
              sessionStorage.removeItem('token');
              sessionStorage.removeItem('rol');
              this.route.navigate(['/login']);
            }else{
              //imprimir mensaje
              this.presenters.presentAlertFail(info.mensaje);
            }
          }
        },(error)=>{
          this.spinner.hide();
          this.presenters.presentAlertFail('No hay conexion con el servidor, intente mas tarde.');
        }
      );
  }
    
  

}
