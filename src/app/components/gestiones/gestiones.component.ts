import { NavbarService } from 'app/services/navbar.service';
import { Component, OnInit } from '@angular/core';
import { AdministradorServiceService } from 'app/services/administrador-service.service';
import { ParametersService } from 'app/services/parameters.service';
import { PresentersService } from 'app/services/presenters.service';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import  Swal  from 'sweetalert2';
@Component({
  selector: 'app-gestiones',
  templateUrl: './gestiones.component.html',
  styleUrls: ['./gestiones.component.css']
})
export class GestionesComponent implements OnInit {

  gestiones:any;
  cantidad:number;
  ano:string;
  copia_gestiones_original:any;
  page:number=1;
  constructor(
    public navbar:NavbarService,
    public route:Router,
    public aservice:AdministradorServiceService,
    public parameters:ParametersService,
    public presenters:PresentersService,
    public spinner:NgxSpinnerService
    ) {

   }

  ngOnInit() {
    this.navbar.show();
    this.obtenerGestiones();
  }

  obtenerGestiones()
  {
    let info:any;
    this.spinner.show();
    let token=sessionStorage.getItem('token');
    this.aservice.obtenerGestiones(token).subscribe(
      (data)=>{
        this.spinner.hide();
        data=data.json(); //parse to json
        info=Object.assign(data);
        console.log('info',info);
        
        if(info.estado=="exito")
        {
          //exito
          if(info.data!=null)
          {
            this.gestiones=info.data;
            this.copia_gestiones_original=this.gestiones;
            this.cantidad=this.gestiones.length;
          }else{
            this.gestiones=[];
            this.cantidad=0;
          }
          
        }else{
          //error
          if(info.mensaje=="el token ha expirado."){
            //mandar a login
            this.presenters.presentAlertInfo('La sesion expiro','Por favor vuelva a ingresar.');
            this.parameters.setToken('');
            this.parameters.setRol('');
            sessionStorage.removeItem('token');
            sessionStorage.removeItem('rol');
            this.route.navigate(['/login']);
          }else{
            //imprimir mensaje
            this.presenters.presentAlertFail(info.mensaje);
          }
        }

      },(error)=>{
        //error en la conexion
        this.spinner.hide();
        this.presenters.presentAlertFail('No hay conexion con el servidor, intente mas tarde.');
      }
    );
  }

  irRegistro()
  {
    this.route.navigate(['/registro-gestion']);
  }


  eliminarGestion(gestion_id)
  {
    console.log(gestion_id);
    let info:any;
    this.spinner.show();
    let token=sessionStorage.getItem('token');
    this.aservice.eliminarGestion(token,gestion_id).subscribe(
      (data)=>{
        this.spinner.hide();
        data=data.json(); //parse to json
        info=Object.assign(data);
        console.log('info',info);
        
        if(info.estado=="exito")
        {
          //exito
          this.presenters.presentAlertSuccess('Gestion eliminada con exito.');
          setTimeout(()=>{
            window.location.reload();
          },1000);
          
        }else{
          //error
          if(info.mensaje=="el token ha expirado."){
            //mandar a login
            this.presenters.presentAlertInfo('La sesion expiro','Por favor vuelva a ingresar.');
            this.parameters.setToken('');
            this.parameters.setRol('');
            sessionStorage.removeItem('token');
            sessionStorage.removeItem('rol');
            this.route.navigate(['/login']);
          }else{
            //imprimir mensaje
            this.presenters.presentAlertFail(info.mensaje);
          }
        }

      },(error)=>{
        //error en la conexion
        this.spinner.hide();
        this.presenters.presentAlertFail('No hay conexion con el servidor, intente mas tarde.');
      }
    );
    
  }

  activarGestion(gestion_id)
  {
    console.log(gestion_id);
    let info:any;
    this.spinner.show();
    let token=sessionStorage.getItem('token');
    this.aservice.activarGestion(token,gestion_id).subscribe(
      (data)=>{
        this.spinner.hide();
        data=data.json(); //parse to json
        info=Object.assign(data);
        console.log('info',info);
        
        if(info.estado=="exito")
        {
          //exito
          this.presenters.presentAlertSuccess('Gestion activada con exito.');
          setTimeout(()=>{
            window.location.reload();
          },1000);
          
        }else{
          //error
          if(info.mensaje=="el token ha expirado."){
            //mandar a login
            this.presenters.presentAlertInfo('La sesion expiro','Por favor vuelva a ingresar.');
            this.parameters.setToken('');
            this.parameters.setRol('');
            sessionStorage.removeItem('token');
            sessionStorage.removeItem('rol');
            this.route.navigate(['/login']);
          }else{
            //imprimir mensaje
            this.presenters.presentAlertFail(info.mensaje);
          }
        }

      },(error)=>{
        //error en la conexion
        this.spinner.hide();
        this.presenters.presentAlertFail('No hay conexion con el servidor, intente mas tarde.');
      }
    );
    
  }

  copia_gestiones:any;
  resultados:any=[];
  buscar()
  {
    console.log(this.ano);
    if(this.ano==null || this.ano==''){
      console.log(this.copia_gestiones);
      
      this.gestiones=this.copia_gestiones_original;
      this.cantidad=this.copia_gestiones_original.length;
    }else{
      setTimeout(()=>{
        this.copia_gestiones=this.copia_gestiones_original;
        console.log(this.copia_gestiones);
        
        for (let i = 0; i < this.copia_gestiones.length; i++) {
          let ano=this.copia_gestiones[i].ano;
          if(ano==this.ano)
          {
            this.resultados.push(this.copia_gestiones[i]);
          } 
        }
        
        this.cantidad=this.resultados.length;
        this.gestiones=this.resultados;
        console.log(this.gestiones);
        this.resultados=[];
      },1000);
    }
  }

  preguntar(usuario)
  {
    Swal.fire({
      title: "¿Esta seguro de que el o la estudiante "+usuario.nombre+" "+usuario.apellidos+" termino su carrera universitaria?",
      text: "Una vez retirado debe entregar el dinero.",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, hazlo!'
    }).then((result) => {
      if (result.value) {
        //this.retirar(usuario);
      }
    });
  }
  
  irAgregar(gestion)
  {
    this.parameters.setGestion(gestion);
    this.route.navigate(['/agregar-mensualidad']);
  }
  irVer(gestion)
  {
    this.parameters.setGestion(gestion);
    this.route.navigate(['/ver-mensualidad']);
  }
  verPagosGestion(gestion)
  {
    this.parameters.setGestion(gestion);
    this.route.navigate(['/pagos-gestion']);
  }
}
