import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { NavbarService } from 'app/services/navbar.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ParametersService } from 'app/services/parameters.service';
import { PresentersService } from 'app/services/presenters.service';
import { AdministradorServiceService } from 'app/services/administrador-service.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-recuperar-password',
  templateUrl: './recuperar-password.component.html',
  styleUrls: ['./recuperar-password.component.css']
})
export class RecuperarPasswordComponent implements OnInit {

  form1:FormGroup;
  form2:FormGroup;
  ocultar=false;
  verLoad:boolean=false;
  constructor(public nav:NavbarService,
    public route:Router,
    public aservice:AdministradorServiceService,
    public parameters:ParametersService,
    public presenters:PresentersService,
    public spinner:NgxSpinnerService) {
    this.form1= new FormGroup({
      'carnet': new FormControl('',[Validators.required])
    });
    this.form2= new FormGroup({
      'carnet': new FormControl(''),
      'codigo': new FormControl('',[Validators.required])
    });
   }

  ngOnInit() {
    this.nav.hide();
  }

  enviarCodigo()
  {
    let carnet=this.form1.controls['carnet'].value;
    this.enviar(carnet);
    
  }

  recuperar()
  {
    this.form2.patchValue({
      'carnet':this.form1.controls['carnet'].value
    });
    console.log('data',this.form2.value);
    let carnet=this.form2.controls['carnet'].value;
    let codigo=this.form2.controls['codigo'].value;
    console.log('carnet enviado:',carnet);
    this.verificarCodigo(carnet,codigo);
    
  }

  irLogin()
  {
    this.route.navigate(['/login']);
  }

  enviar(carnet)
  {
    console.log(carnet);
    let info:any;
    this.spinner.show();
    this.verLoad=true;
    let token=sessionStorage.getItem('token');
    this.aservice.obtenerCodigoResetPassword(token,carnet).subscribe(
      (data)=>{
        this.spinner.hide();
        this.verLoad=false;
        data=data.json(); //parse to json
        info=Object.assign(data);
        console.log('info',info);
        
        if(info.estado=="exito")
        {
          //exito
          this.presenters.presentAlertSuccess('Se envio un codigo de validacion a tu correo.');
          this.form1.controls['carnet'].disable();
          this.ocultar=true;
        }else{
          //error
          if(info.mensaje=="el token ha expirado."){
            //mandar a login
            this.presenters.presentAlertInfo('La sesion expiro','Por favor vuelva a ingresar.');
            this.parameters.setToken('');
            this.parameters.setRol('');
            sessionStorage.removeItem('token');
            sessionStorage.removeItem('rol');
            this.route.navigate(['/login']);
          }else{
            //imprimir mensaje
            this.presenters.presentAlertFail(info.mensaje);
          }
        }

      },(error)=>{
        //error en la conexion
        this.spinner.hide();
        this.verLoad=false;
        this.presenters.presentAlertFail('No hay conexion con el servidor, intente mas tarde.');
      }
    );
  }

  verificarCodigo(carnet:number,codigo:string)
  {
    console.log(carnet+' '+codigo);
    let info:any;
    this.spinner.show();
    let token=sessionStorage.getItem('token');
    this.aservice.verificarCodigo(token,carnet,codigo).subscribe(
      (data)=>{
        this.spinner.hide();
        data=data.json(); //parse to json
        info=Object.assign(data);
        console.log('info',info);
        
        if(info.estado=="exito")
        {
          //exito
          this.parameters.setCarnetResetPass(carnet);
          this.route.navigate(['nueva-password']);
        }else{
          //error
          if(info.mensaje=="el token ha expirado."){
            //mandar a login
            this.presenters.presentAlertInfo('La sesion expiro','Por favor vuelva a ingresar.');
            this.parameters.setToken('');
            this.parameters.setRol('');
            sessionStorage.removeItem('token');
            sessionStorage.removeItem('rol');
            this.route.navigate(['/login']);
          }else{
            //imprimir mensaje
            this.presenters.presentAlertFail(info.mensaje);
          }
        }

      },(error)=>{
        //error en la conexion
        this.spinner.hide();
        this.presenters.presentAlertFail('No hay conexion con el servidor, intente mas tarde.');
      }
    );
  }

}
