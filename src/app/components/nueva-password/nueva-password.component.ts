import { Component, OnInit } from '@angular/core';
import { NavbarService } from 'app/services/navbar.service';
import { PresentersService } from 'app/services/presenters.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ParametersService } from 'app/services/parameters.service';
import { AdministradorServiceService } from 'app/services/administrador-service.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-nueva-password',
  templateUrl: './nueva-password.component.html',
  styleUrls: ['./nueva-password.component.css']
})
export class NuevaPasswordComponent implements OnInit {

  cerrarRules=true;
  form:FormGroup
  constructor(public nav:NavbarService,
    public presenters:PresentersService,
    public parameters:ParametersService,
    public aservice:AdministradorServiceService,
    public route:Router,
    public spinner:NgxSpinnerService) { 
    this.form= new FormGroup({
      'pass1': new FormControl('',[Validators.required]),
      'pass2': new FormControl('',[Validators.required])
    });
  }

  ngOnInit() {
    this.nav.hide();
    console.log('carnet',this.parameters.getCarnetResetPass());
    
  }

  cambiar()
  {
    console.log();
    
    if(this.form.controls['pass1'].value==this.form.controls['pass2'].value)
    {
      let es_valido=this.validarPassword(this.form.controls['pass1'].value);
      if (es_valido) {
        let pass1=this.form.controls['pass1'].value;
        let pass2=this.form.controls['pass2'].value;
        let carnet=this.parameters.getCarnetResetPass();
        this.cambiarPass(pass1,pass2,carnet);
      } else {
        this.presenters.presentAlertFail('La password no cumple con los requisitos requeridos.');
      }
    }else{
      this.presenters.presentAlertFail('Las passwords no son iguales.');
    }
  }

  irAtras()
  {
    this.route.navigate(['/recuperar-password']);
  }

  cambiarPass(pass1,pass2,carnet)
  {
    let info:any;
    this.spinner.show();
    let token=sessionStorage.getItem('token');
    this.aservice.cambiarPasswordReset(carnet,pass1,pass2).subscribe(
      (data)=>{
        this.spinner.hide();
        data=data.json(); //parse to json
        info=Object.assign(data);
        console.log('info',info);
        
        if(info.estado=="exito")
        {
          //exito
          this.presenters.presentAlertSuccess('Se cambio la password correctamente.');
          this.route.navigate(['/login']);
        }else{
          //error
          if(info.mensaje=="el token ha expirado."){
            //mandar a login
            this.presenters.presentAlertInfo('La sesion expiro','Por favor vuelva a ingresar.');
            this.parameters.setToken('');
            this.parameters.setRol('');
            sessionStorage.removeItem('token');
            sessionStorage.removeItem('rol');
            this.route.navigate(['/login']);
          }else{
            //imprimir mensaje
            this.presenters.presentAlertFail(info.mensaje);
          }
        }

      },(error)=>{
        //error en la conexion
        this.spinner.hide();
        this.presenters.presentAlertFail('No hay conexion con el servidor, intente mas tarde.');
      }
    );
  }

  cerrar()
  {
    console.log('se cerro');
    this.cerrarRules=false;
  }
  validarPassword(password:string)
  {
    // se pone /expresion_regular/.test(palabra a examinar) y devuelve un booleano
    return /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{10,}$/.test(password);
  }

}
