import { NavbarService } from 'app/services/navbar.service';
import { Component, OnInit } from '@angular/core';
import { AdministradorServiceService } from 'app/services/administrador-service.service';
import { ParametersService } from 'app/services/parameters.service';
import { PresentersService } from 'app/services/presenters.service';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { NgxSpinnerService } from 'ngx-spinner';
@Component({
  selector: 'app-retiro',
  templateUrl: './retiro.component.html',
  styleUrls: ['./retiro.component.css']
})
export class RetiroComponent implements OnInit {

  mostrar=false;
  mostrarSpinner=false;
  carnet:number;
  dataUsuario:any;
  total:number=0;
  constructor(public nav:NavbarService,
    public aservice:AdministradorServiceService,
    public parameters:ParametersService,
    public presenters:PresentersService,
    public route:Router,
    public spinner:NgxSpinnerService) { }

  ngOnInit() {
    this.nav.show();
  }

  buscar()
  {
    if(this.carnet==undefined || this.carnet==0)
    {
      this.presenters.presentAlertFail('Carnet invalido.');
    }else{
      this.obtenerInfoRetiro();
      
      
    }
  }

  obtenerInfoRetiro()
  {
    this.total=0;
    let info:any;
    this.spinner.show();
    this.mostrarSpinner=true;
    let token=sessionStorage.getItem('token');
    this.aservice.obtenerInfoRetiro(token,this.carnet).subscribe(
      (data)=>{
        this.spinner.hide();
        data=data.json(); //parse to json
        info=Object.assign(data);
        console.log('info',info);
        
        if(info.estado=="exito")
        {
          //exito
          this.mostrarSpinner=false;
          this.mostrar=true;
          this.dataUsuario=info.data;
          for (let i = 0; i < info.data.cuentas.length; i++) {
            this.total=this.total+info.data.cuentas[i].saldo;
          }
          console.log(this.total);
          
        }else{
          //error
          if(info.mensaje=="el token ha expirado."){
            //mandar a login
            this.mostrarSpinner=false;
            this.presenters.presentAlertInfo('La sesion expiro','Por favor vuelva a ingresar.');
            this.parameters.setToken('');
            this.parameters.setRol('');
            sessionStorage.removeItem('token');
            sessionStorage.removeItem('rol');
            this.route.navigate(['/login']);
          }else{
            //imprimir mensaje
            this.mostrarSpinner=false;
            this.presenters.presentAlertFail(info.mensaje);
          }
        }

      },(error)=>{
        //error en la conexion
        this.spinner.hide();
        this.mostrarSpinner=false;
        this.presenters.presentAlertFail('No hay conexion con el servidor, intente mas tarde.');
      }
    );
  }

  preguntar(numero_cuenta)
  {
    Swal.fire({
      title: "Esta seguro de realizar el retiro?",
      text: "Una vez retirado debe entregar el dinero.",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, hazlo!'
    }).then((result) => {
      if (result.value) {
        this.retirar(numero_cuenta);
      }
    });
  }

  retirar(numero_cuenta)
  {
    console.log(numero_cuenta);
    let info:any;
    this.spinner.show();
    let token=sessionStorage.getItem('token');
    this.aservice.retirar(token,numero_cuenta).subscribe(
      (data)=>{
        this.spinner.hide();
        data=data.json(); //parse to json
        info=Object.assign(data);
        console.log('info',info);
        
        if(info.estado=="exito")
        {
          //exito
          this.presenters.presentAlertSuccess('Se realizo el retiro correctamente.');
          this.buscar();
        }else{
          //error
          if(info.mensaje=="el token ha expirado."){
            //mandar a login
            this.presenters.presentAlertInfo('La sesion expiro','Por favor vuelva a ingresar.');
            this.parameters.setToken('');
            this.parameters.setRol('');
            sessionStorage.removeItem('token');
            sessionStorage.removeItem('rol');
            this.route.navigate(['/login']);
          }else{
            //imprimir mensaje
            this.presenters.presentAlertFail(info.mensaje);
          }
        }

      },(error)=>{
        //error en la conexion
        this.spinner.hide();
        this.presenters.presentAlertFail('No hay conexion con el servidor, intente mas tarde.');
      }
    );
  }

}
