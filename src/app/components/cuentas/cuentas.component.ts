import { NavbarService } from 'app/services/navbar.service';
import { Component, OnInit } from '@angular/core';
import { AdministradorServiceService } from 'app/services/administrador-service.service';
import { ParametersService } from 'app/services/parameters.service';
import { PresentersService } from 'app/services/presenters.service';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import  Swal  from 'sweetalert2';
@Component({
  selector: 'app-cuentas',
  templateUrl: './cuentas.component.html',
  styleUrls: ['./cuentas.component.css']
})
export class CuentasComponent implements OnInit {

  cuentas:any;
  cantidad:number;
  numero_cuenta:number;
  copia_cuentas_original:any;
  page:number=1;
  constructor(
    public navbar:NavbarService,
    public route:Router,
    public aservice:AdministradorServiceService,
    public parameters:ParametersService,
    public presenters:PresentersService,
    public spinner:NgxSpinnerService
    ) {

   }

  ngOnInit() {
    this.navbar.show();
    this.obtenerCuentas();
  }

  obtenerCuentas()
  {
    let info:any;
    this.spinner.show();
    let token=sessionStorage.getItem('token');
    this.aservice.listarCuentas(token).subscribe(
      (data)=>{
        this.spinner.hide();
        data=data.json(); //parse to json
        info=Object.assign(data);
        console.log('info',info);
        
        if(info.estado=="exito")
        {
          //exito
          if(info.data!=null)
          {
            this.cuentas=info.data;
            this.copia_cuentas_original=this.cuentas;
            this.cantidad=this.cuentas.length;
          }else{
            this.cuentas=[];
            this.cantidad=0;
          }
          
        }else{
          //error
          if(info.mensaje=="el token ha expirado."){
            //mandar a login
            this.presenters.presentAlertInfo('La sesion expiro','Por favor vuelva a ingresar.');
            this.parameters.setToken('');
            this.parameters.setRol('');
            sessionStorage.removeItem('token');
            sessionStorage.removeItem('rol');
            this.route.navigate(['/login']);
          }else{
            //imprimir mensaje
            this.presenters.presentAlertFail(info.mensaje);
          }
        }

      },(error)=>{
        //error en la conexion
        this.spinner.hide();
        this.presenters.presentAlertFail('No hay conexion con el servidor, intente mas tarde.');
      }
    );
  }


  eliminarCuenta(cuenta_id)
  {
    console.log(cuenta_id);
    let info:any;
    this.spinner.show();
    let token=sessionStorage.getItem('token');
    this.aservice.eliminarCuenta(token,cuenta_id).subscribe(
      (data)=>{
        this.spinner.hide();
        data=data.json(); //parse to json
        info=Object.assign(data);
        console.log('info',info);
        
        if(info.estado=="exito")
        {
          //exito
          this.presenters.presentAlertSuccess('Cuenta eliminada con exito.');
          setTimeout(()=>{
            window.location.reload();
          },1000);
          
        }else{
          //error
          if(info.mensaje=="el token ha expirado."){
            //mandar a login
            this.presenters.presentAlertInfo('La sesion expiro','Por favor vuelva a ingresar.');
            this.parameters.setToken('');
            this.parameters.setRol('');
            sessionStorage.removeItem('token');
            sessionStorage.removeItem('rol');
            this.route.navigate(['/login']);
          }else{
            //imprimir mensaje
            this.presenters.presentAlertFail(info.mensaje);
          }
        }

      },(error)=>{
        //error en la conexion
        this.spinner.hide();
        this.presenters.presentAlertFail('No hay conexion con el servidor, intente mas tarde.');
      }
    );
    
  }

  activarCuenta(cuenta_id)
  {
    console.log(cuenta_id);
    let info:any;
    this.spinner.show();
    let token=sessionStorage.getItem('token');
    this.aservice.activarCuenta(token,cuenta_id).subscribe(
      (data)=>{
        this.spinner.hide();
        data=data.json(); //parse to json
        info=Object.assign(data);
        console.log('info',info);
        
        if(info.estado=="exito")
        {
          //exito
          this.presenters.presentAlertSuccess('Cuenta activada con exito.');
          setTimeout(()=>{
            window.location.reload();
          },1000);
          
        }else{
          //error
          if(info.mensaje=="el token ha expirado."){
            //mandar a login
            this.presenters.presentAlertInfo('La sesion expiro','Por favor vuelva a ingresar.');
            this.parameters.setToken('');
            this.parameters.setRol('');
            sessionStorage.removeItem('token');
            sessionStorage.removeItem('rol');
            this.route.navigate(['/login']);
          }else{
            //imprimir mensaje
            this.presenters.presentAlertFail(info.mensaje);
          }
        }

      },(error)=>{
        //error en la conexion
        this.spinner.hide();
        this.presenters.presentAlertFail('No hay conexion con el servidor, intente mas tarde.');
      }
    );
    
  }

  copia_cuentas:any;
  resultados:any=[];
  buscar()
  {
    console.log(this.numero_cuenta);
    if(this.numero_cuenta==null){
      console.log(this.copia_cuentas);
      
      this.cuentas=this.copia_cuentas_original;
      this.cantidad=this.copia_cuentas_original.length;
    }else{
      setTimeout(()=>{
        this.copia_cuentas=this.copia_cuentas_original;
        console.log(this.copia_cuentas);
        
        for (let i = 0; i < this.copia_cuentas.length; i++) {
          let cuenta=this.copia_cuentas[i].numero_cuenta;
          if(cuenta==this.numero_cuenta)
          {
            this.resultados.push(this.copia_cuentas[i]);
          } 
        }
        
        this.cantidad=this.resultados.length;
        this.cuentas=this.resultados;
        console.log(this.cuentas);
        this.resultados=[];
      },1000);
    }
  }

  preguntar(usuario)
  {
    Swal.fire({
      title: "¿Esta seguro de que el o la estudiante "+usuario.nombre+" "+usuario.apellidos+" termino su carrera universitaria?",
      text: "Una vez retirado debe entregar el dinero.",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, hazlo!'
    }).then((result) => {
      if (result.value) {
        this.retirar(usuario);
      }
    });
  }
  retirar(usuario)
  {
    console.log('usuario',usuario);
    let info:any;
    this.spinner.show();
    let token=sessionStorage.getItem('token');
    this.aservice.retiroCuentaEstudiante(token,usuario.usuario_id).subscribe(
      (data)=>{
        this.spinner.hide();
        data=data.json(); //parse to json
        info=Object.assign(data);
        console.log('info',info);
        
        if(info.estado=="exito")
        {
          //exito
          this.presenters.presentAlertSuccess('Retiro de cuenta con exito.');
          setTimeout(()=>{
            window.location.reload();
          },1000);
          
        }else{
          //error
          if(info.mensaje=="el token ha expirado."){
            //mandar a login
            this.presenters.presentAlertInfo('La sesion expiro','Por favor vuelva a ingresar.');
            this.parameters.setToken('');
            this.parameters.setRol('');
            sessionStorage.removeItem('token');
            sessionStorage.removeItem('rol');
            this.route.navigate(['/login']);
          }else{
            //imprimir mensaje
            this.presenters.presentAlertFail(info.mensaje);
          }
        }

      },(error)=>{
        //error en la conexion
        this.spinner.hide();
        this.presenters.presentAlertFail('No hay conexion con el servidor, intente mas tarde.');
      }
    );
  }

}
