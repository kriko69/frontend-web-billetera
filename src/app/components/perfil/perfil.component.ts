import { Router } from '@angular/router';
import { NavbarService } from 'app/services/navbar.service';
import { Component, OnInit } from '@angular/core';
import { NgForm, FormControl, FormGroup,Validators } from '@angular/forms';
import { AdministradorServiceService } from 'app/services/administrador-service.service';
import { ParametersService } from 'app/services/parameters.service';
import { PresentersService } from 'app/services/presenters.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styleUrls: ['./perfil.component.css']
})
export class PerfilComponent implements OnInit {

  form:FormGroup;
  ver=true;
  dataPerfil:any;
  urlFoto;
  rol;
  poderGuardar=true;
  constructor(public nav:NavbarService,
    public route:Router,
    public aservice:AdministradorServiceService,
    public parameters:ParametersService,
    public presenters:PresentersService,
    public spinner:NgxSpinnerService) {

    this.form= new FormGroup({
      'nombre': new FormControl({value:'',disabled:this.ver},[Validators.required]),
      'apellidos': new FormControl({value:'',disabled:this.ver},[Validators.required]),
      'carnet': new FormControl({value:'',disabled:this.ver},[Validators.required]),
      'correo': new FormControl({value:'',disabled:this.ver},[Validators.required, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$')]),
      'telefono': new FormControl({value:'',disabled:this.ver},[Validators.required]),
      'fecha_nacimiento': new FormControl({value:'',disabled:this.ver},[Validators.required]),
      'carrera':new FormControl('sin carrera')
    });
    this.obtenerPerfil();
    this.rol=sessionStorage.getItem('rol');
    
    if(this.rol=='administrador')
    {
      this.urlFoto=this.parameters.fotoAdministrador;
    }else{
      if(this.rol=='superadmin')
      {
        this.urlFoto=this.parameters.fotoSuperAdministrador;
      }
    }


   }


  ngOnInit() {    
    this.nav.show();
  }

  registrar()
  {
    console.log(this.form.value);
    
    let info:any;
    this.spinner.show();
    let token=sessionStorage.getItem('token');
    this.aservice.editarMiPerifl(token,this.form.value).subscribe(
      (data)=>{
        this.spinner.hide();
        data=data.json(); //parse to json
        info=Object.assign(data);
        console.log('info',info);
        
        if(info.estado=="exito")
        {
          //exito
          this.presenters.presentAlertSuccess('Se actualizo el perfil correctamente');
          
        }else{
          //error
          if(info.mensaje=="el token ha expirado."){
            //mandar a login
            this.presenters.presentAlertInfo('La sesion expiro','Por favor vuelva a ingresar.');
            this.parameters.setToken('');
            this.parameters.setRol('');
            sessionStorage.removeItem('token');
            sessionStorage.removeItem('rol');
            this.route.navigate(['/login']);
          }else{
            //imprimir mensaje
            this.presenters.presentAlertFail(info.mensaje);
          }
        }

      },(error)=>{
        this.spinner.hide();
        //error en la conexion
        this.presenters.presentAlertFail('No hay conexion con el servidor, intente mas tarde.');
      }
    );
  }

  editarDatos()
  {
    console.log('cambio');
    this.ver=!this.ver;
    if(!this.ver)
    {
      this.form.controls['nombre'].enable();
      this.form.controls['apellidos'].enable();
      this.form.controls['carnet'].disable();
      this.form.controls['correo'].enable();
      this.form.controls['telefono'].enable();
      this.form.controls['fecha_nacimiento'].enable();
    }else{
      this.form.controls['nombre'].disable();
      this.form.controls['apellidos'].disable();
      this.form.controls['carnet'].disable();
      this.form.controls['correo'].disable();
      this.form.controls['telefono'].disable();
      this.form.controls['fecha_nacimiento'].disable();
    }
  }

  cambioPass(){
    this.parameters.setCarnetAdmin(this.form.controls["carnet"].value);
    this.route.navigate(['/cambio-password']);
  }

  obtenerPerfil()
  {
    let info:any;
    this.spinner.show();
    let token=sessionStorage.getItem('token');
    this.aservice.perfil(token).subscribe(
      (data)=>{
        this.spinner.hide();
        data=data.json(); //parse to json
        info=Object.assign(data);
        console.log('info',info);
        
        if(info.estado=="exito")
        {
          //exito
          this.dataPerfil=info.data;
          this.urlFoto=this.urlFoto+info.data.imagen;
          console.log(this.urlFoto);
          
          this.form.patchValue({
            'nombre': this.dataPerfil.nombre,
            'apellidos': this.dataPerfil.apellidos,
            'carnet': this.dataPerfil.carnet,
            'correo': this.dataPerfil.correo,
            'telefono': this.dataPerfil.telefono,
            'fecha_nacimiento': this.dataPerfil.fecha_nacimiento
          });
          
        }else{
          //error
          if(info.mensaje=="el token ha expirado."){
            //mandar a login
            this.presenters.presentAlertInfo('La sesion expiro','Por favor vuelva a ingresar.');
            this.parameters.setToken('');
            this.parameters.setRol('');
            sessionStorage.removeItem('token');
            sessionStorage.removeItem('rol');
            this.route.navigate(['/login']);
          }else{
            //imprimir mensaje
            this.presenters.presentAlertFail(info.mensaje);
          }
        }

      },(error)=>{
        //error en la conexion
        this.spinner.hide();
        this.presenters.presentAlertFail('No hay conexion con el servidor, intente mas tarde.');
      }
    );
  }

  getBaseUrl ()  {
    var file = document.querySelector('input[type=file]')['files'][0];
    console.log(file);
    if(file)
    {
      this.poderGuardar=false;
      let f=document.getElementById('fotito');
      var reader = new FileReader();
      var baseString;
      
      reader.onloadend = function () {
          baseString = reader.result;
          document.getElementById('fotito').setAttribute('src',reader.result);
          baseString =baseString.replace('data:image/png;base64,','');
          baseString =baseString.replace('data:image/jpg;base64,','');
          baseString =baseString.replace('data:image/jpeg;base64,','');
        //console.log(baseString);
       
      };
      reader.readAsDataURL(file);
    }else{
      this.poderGuardar=true;

      document.getElementById('fotito').setAttribute('src',this.urlFoto);
    }
    
    
    //console.log('foto',this.urlFoto);
    
  }

  guardarFoto()
  {
    let base64 = document.getElementById('fotito').getAttribute('src');
    base64 =base64.replace('data:image/png;base64,','');
        base64 =base64.replace('data:image/jpg;base64,','');
        base64 =base64.replace('data:image/jpeg;base64,','');
       //console.log(base64);
    let rol=sessionStorage.getItem('rol');
    let token=sessionStorage.getItem('token');
    //console.log(rol);
    //console.log(token);
    this.save(base64,rol);

    
    
  }

  save(image,rol)
  {
    let info:any;
    this.spinner.show();
    let token=sessionStorage.getItem('token');
    this.aservice.actualizarFotoPerfil(token,image,rol).subscribe(
      (data)=>{
        this.spinner.hide();
        data=data.json(); //parse to json
        info=Object.assign(data);
        console.log('info',info);
        
        if(info.estado=="exito")
        {
          //exito
          this.presenters.presentAlertSuccess('Se actualizo la foto de perfil con exito.')          
        }else{
          //error
          if(info.mensaje=="el token ha expirado."){
            //mandar a login
            this.presenters.presentAlertInfo('La sesion expiro','Por favor vuelva a ingresar.');
            this.parameters.setToken('');
            this.parameters.setRol('');
            sessionStorage.removeItem('token');
            sessionStorage.removeItem('rol');
            this.route.navigate(['/login']);
          }else{
            //imprimir mensaje
            this.presenters.presentAlertFail(info.mensaje);
          }
        }

      },(error)=>{
        //error en la conexion
        this.spinner.hide();
        this.presenters.presentAlertFail('No hay conexion con el servidor, intente mas tarde.');
      }
    );
  }

  eliminarFoto()
  {
    let info:any;
    this.spinner.show();
    let token=sessionStorage.getItem('token');
    this.aservice.eliminarFotoPerfil(token).subscribe(
      (data)=>{
        this.spinner.hide();
        data=data.json(); //parse to json
        info=Object.assign(data);
        console.log('info',info);
        
        if(info.estado=="exito")
        {
          //exito
          this.presenters.presentAlertSuccess('Se elimino la foto de perfil con exito.')          
        }else{
          //error
          if(info.mensaje=="el token ha expirado."){
            //mandar a login
            this.presenters.presentAlertInfo('La sesion expiro','Por favor vuelva a ingresar.');
            this.parameters.setToken('');
            this.parameters.setRol('');
            sessionStorage.removeItem('token');
            sessionStorage.removeItem('rol');
            this.route.navigate(['/login']);
          }else{
            //imprimir mensaje
            this.presenters.presentAlertFail(info.mensaje);
          }
        }

      },(error)=>{
        //error en la conexion
        this.spinner.hide();
        this.presenters.presentAlertFail('No hay conexion con el servidor, intente mas tarde.');
      }
    );
  }

  

}
