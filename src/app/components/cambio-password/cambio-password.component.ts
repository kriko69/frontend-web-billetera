import { FormGroup, FormControl, Validators } from '@angular/forms';
import { NavbarService } from 'app/services/navbar.service';
import { Component, OnInit } from '@angular/core';
import { PresentersService } from 'app/services/presenters.service';
import { ParametersService } from 'app/services/parameters.service';
import { AdministradorServiceService } from 'app/services/administrador-service.service';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-cambio-password',
  templateUrl: './cambio-password.component.html',
  styleUrls: ['./cambio-password.component.css']
})
export class CambioPasswordComponent implements OnInit {

  form:FormGroup;
  carnet:number;
  verpass1=false;
  verpass2=false;
  input1="password";
  input2="password";
  cerrarRules=true;
  constructor(public nav:NavbarService,
    public presenters:PresentersService,
    public parameters:ParametersService,
    public aservice:AdministradorServiceService,
    public route:Router,
    public spinner:NgxSpinnerService) { 
    this.form= new FormGroup({
      'pass1': new FormControl('',[Validators.required]),
      'pass2': new FormControl('',[Validators.required])
    });
    this.carnet=this.parameters.getCarnetAdmin();

  }

  ngOnInit() {
    this.nav.show();
  }

  cambiar()
  {
    console.log();
    
    if(this.form.controls['pass1'].value==this.form.controls['pass2'].value)
    {
      this.presenters.presentAlertSuccess('las password son iguales.');
      let es_valido=this.validarPassword(this.form.controls['pass1'].value);
      if (es_valido) {
        this.actualizarPassword(this.form.controls['pass1'].value,this.form.controls['pass2'].value);
      } else {
        this.presenters.presentAlertFail('La password no cumple con los requisitos requeridos.');
      }
    }else{
      this.presenters.presentAlertFail('Las passwords no son iguales.');
    }
  }

  actualizarPassword(pass1:string,pass2:string)
  {
    let info:any;
    this.spinner.show();
    console.log('carnet',this.carnet);
    
    let token=sessionStorage.getItem('token');
    this.aservice.cambiarMiPassword(token,this.carnet,pass1,pass2).subscribe(
      (data)=>{
        this.spinner.hide();
        data=data.json(); //parse to json
        info=Object.assign(data);
        console.log('info',info);
        
        if(info.estado=="exito")
        {
          //exito
          this.presenters.presentAlertSuccess('Se actualizo la password correctamente.');
        }else{
          //error
          if(info.mensaje=="el token ha expirado."){
            //mandar a login
            this.presenters.presentAlertInfo('La sesion expiro','Por favor vuelva a ingresar.');
            this.parameters.setToken('');
            this.parameters.setRol('');
            sessionStorage.removeItem('token');
            sessionStorage.removeItem('rol');
            this.route.navigate(['/login']);
          }else{
            //imprimir mensaje
            this.presenters.presentAlertFail(info.mensaje);
          }
        }

      },(error)=>{
        //error en la conexion
        this.spinner.hide();
        this.presenters.presentAlertFail('No hay conexion con el servidor, intente mas tarde.');
      }
    );
  }

  verPassword1()
  {
    this.verpass1=!this.verpass1;
    if(this.verpass1)
    {
      this.input1="text";
    }else{
      this.input1="password"
    }
  }
  verPassword2()
  {
    this.verpass2=!this.verpass2;
    if(this.verpass2)
    {
      this.input2="text";
    }else{
      this.input2="password"
    }
  }

  cerrar()
  {
    console.log('se cerro');
    this.cerrarRules=false;
  }
  validarPassword(password:string)
  {
    // se pone /expresion_regular/.test(palabra a examinar) y devuelve un booleano
    return /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{10,}$/.test(password);
  }


}
