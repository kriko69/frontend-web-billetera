import { Component, OnInit } from '@angular/core';
import { NgForm, FormControl, FormGroup,Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AdministradorServiceService } from 'app/services/administrador-service.service';
import { ParametersService } from 'app/services/parameters.service';
import { NavbarService } from 'app/services/navbar.service';
import { PresentersService } from 'app/services/presenters.service';

import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  form:FormGroup;
  passtype="password";
  icono_ojo=true;
  constructor(public router:Router,
    public aservice:AdministradorServiceService,
    public parameters:ParametersService,
    public nav:NavbarService,
    public presenters:PresentersService,
    public spinner:NgxSpinnerService) {
      this.encriptacion();
      this.desencriptar();
    this.form= new FormGroup({
      'carnet': new FormControl('',[Validators.required]),
      'password': new FormControl('',[Validators.required])
    });
    
   }

  ngOnInit() {
    this.nav.hide();
  }

  verPassword()
  {
    if(this.passtype=='password')
    {
      this.passtype='text';
      this.icono_ojo=false;
    }
    else{
      this.passtype='password';
      this.icono_ojo=true;
    }
    
  }
  ingresar()
  {
    console.log(this.form.value); 
    
    let info:any;
    let roles;
    let carnet=this.form.controls['carnet'].value;
    let password=this.form.controls['password'].value;
    this.spinner.show();
    this.aservice.login(carnet,password).subscribe(
      data => {
        this.spinner.hide();
        info= Object.assign(data);
        console.log('info',info);
        
        //this.presenters.quitLoading();
        if(info.estado=="Tokens creados con exito"){
          console.log('token bien', info);
          console.log('token',info["Token-Verificacion"]);
          roles=info.roles.split(",");
          console.log('rol:',roles[0]);
          this.saveToken(info["Token-Verificacion"],roles[0]);
        }
        else{
          this.presenters.presentAlertFail(info.mensaje);
          console.log('error', info); 
          
        }
      }, (error: any)=> {
        this.spinner.hide();
        //this.presenters.quitLoading();
        console.log('error sin conexion', error);
        this.presenters.presentAlertFail('No hay conexion con el servidor, intente mas tarde.');
        }
    );

  }

  recuperar()
  {
    console.log('recuperar');
    this.router.navigate(['/recuperar-password']);
  }

  

  saveToken(token:string,rol:string){
    
    this.parameters.setToken(token);
    this.parameters.setRol(rol);
    sessionStorage.setItem('token',token);
    sessionStorage.setItem('rol',rol);    
    this.goMenu(rol);
  }

  goMenu(rol){
    if(rol=="administrador")
    {
      console.log('es administrador');
      this.parameters.setInicioSesion(true);
      this.router.navigate(['/usuarios']);
    }else{
      if(rol=="superadmin")
      {
        console.log('es administrador');
        this.parameters.setInicioSesion(true);
        this.router.navigate(['/usuarios']);
      }else{
        //no debe ingresar
        //error usuario sin privilegios
        console.log('usuarios sin privilegios');
        this.presenters.presentAlertFail('Usuario no autorizado para ingresar.');
        //limpio
        this.parameters.setToken('');
        this.parameters.setRol('');
        sessionStorage.removeItem('token');
        sessionStorage.removeItem('rol');
        
      }
    }
    
  }

  /*respuestaDataNull()
  {
    this.aservice.login().subscribe(
      (data)=>{
        
        if(data.estado=="exito")
        {
          //exito
          if(data.data==null)
          {
            //exito sin data
            //imprimir mensaje
          }else{
            //exito con data
          }
        }else{
          //error
          //imprimir mensaje
        }

      },(error)=>{
        //error en la conexion
      }
    );
  }
  respuestaSinDataNull()
  {
    let info:any;
    let token=sessionStorage.getItem('token');
    this.aservice.perfil(token).subscribe(
      (data)=>{
        data=data.json(); //parse to json
        info=Object.assign(data);
        console.log('info',info);
        
        if(info.estado=="exito")
        {
          //exito
          //aca se pone si data es null solo cuando se lista y no hay resultados

        }else{
          //error
          if(info.mensaje=="el token ha expirado."){
            //mandar a login
          }else{
            //imprimir mensaje
            //error nomas
          }
        }

      },(error)=>{
        //error en la conexion
      }
    );
  }*/


  encriptacion()
  {
    let data={
      "nombre":"christo6"
    };

    //convirtiendo a string
    data=JSON.parse(JSON.stringify(data));
    console.log('data',data);

    //convirtiendo a json
    let jsonData=JSON.stringify(data);
    console.log('jsonData',jsonData);

    //convirtiendo a base64
    let base64JsonData=btoa(jsonData);
    console.log('base64JsonData',base64JsonData);
    
    //contar signos de =
    let iguales:number=0;
    iguales = base64JsonData.split('=').length - 1; //forma optimizada
    console.log('iguales',iguales);

    //quitamos iguales
    if(iguales!=0)
    {
      base64JsonData=base64JsonData.slice(0,base64JsonData.length-iguales);
    }
    console.log('sin iguales',base64JsonData);
    
    //cada 3 caracteres reemplazaremos un caracter
    let longitudCadena=base64JsonData.length;
    console.log('longitudCadena',longitudCadena);
    
    let cociente=longitudCadena/3;
    cociente=Math.floor(cociente); //redondeamos siempre abajo
    let resto=longitudCadena%3;
    console.log('cociente',cociente);
    console.log('resto',resto);
    let cantidadCaracteresGenerar; //la suma es cuantos caracteres debemos generar randomicamente
    let cadenaAleatoria='';
    if (resto==0) {
      cantidadCaracteresGenerar=cociente+resto;
      console.log('cantidadCaracteresGenerar',cantidadCaracteresGenerar);
      cadenaAleatoria=this.generarCadena(cantidadCaracteresGenerar); //generamos
      console.log('cadenaAleatoria',cadenaAleatoria);
      this.encriptarModulo0(jsonData,iguales,base64JsonData,cadenaAleatoria);
    } else {
      if (resto==1) {
        cantidadCaracteresGenerar=cociente+resto;
        console.log('cantidadCaracteresGenerar',cantidadCaracteresGenerar);
        cadenaAleatoria=this.generarCadena(cantidadCaracteresGenerar); //generamos
        console.log('cadenaAleatoria',cadenaAleatoria);
        this.encriptarModulo1(jsonData,iguales,base64JsonData,cadenaAleatoria);
      } else {
        if(resto==2)
        {
          cantidadCaracteresGenerar=cociente+1;
          console.log('cantidadCaracteresGenerar',cantidadCaracteresGenerar);
          cadenaAleatoria=this.generarCadena(cantidadCaracteresGenerar); //generamos
          console.log('cadenaAleatoria',cadenaAleatoria);
          this.encriptarModulo2(jsonData,iguales,base64JsonData,cadenaAleatoria);
        }else{
          return 'error modulo fuera del rango'
        }
      }
    }
    
    //fin encriptar
  }
  generarCadena(length) {
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
       result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  }

  encriptarModulo0(jsonData,iguales,base64JsonData:string,cadenaAleatoria:string)
  {
    let caracteresReemplazados:string=""; //son los caracteres cada 3
    let nuevaBaseJsonData=""; //cadena sin los caracteres multiples de 3
    let longitud=base64JsonData.length;

    let indice=0;
    for (let i = 0; i < longitud; i++) {
      indice=i+1;
      if(indice%3==0) //es multiplo de 3
      {
        caracteresReemplazados=caracteresReemplazados+base64JsonData.charAt(i); //concatenamos
      }else{
        nuevaBaseJsonData=nuevaBaseJsonData+base64JsonData.charAt(i); //concatenamos
      } 
    }

    let cadenaCambiada="";
    let aux1=0;
    let aux2=2;
    let longitud2=cadenaAleatoria.length;

    //creamos la cadena cambiada
    for (let i = 0; i < longitud2; i++) {
      let slice=nuevaBaseJsonData.slice(aux1,aux2) //tomamos cada dos (0,2) (2,4)...
      let add=slice+cadenaAleatoria.charAt(i); //al slice le agragamos el caracter random
      cadenaCambiada=cadenaCambiada+add; //concatenamos
      aux1=aux2;
      aux2=aux2+2;
    }

    console.log('caracteresReemplazo',caracteresReemplazados);
    console.log('nuevaBaseJsonData',nuevaBaseJsonData);
    console.log('cadenaCambiada',cadenaCambiada);
    
    //partimos la cadena y cambiamos lugares
    let inverso="";
    let primeraMitad="";
    let segundaMitad="";
    let cociente=cadenaCambiada.length/2;
    cociente=Math.floor(cociente);

    if ((cadenaCambiada.length)%2==0) {
      primeraMitad=cadenaCambiada.slice(0,cociente);
      segundaMitad=cadenaCambiada.slice(cociente,cadenaCambiada.length);
      inverso=segundaMitad+primeraMitad;
    } else {
      primeraMitad=cadenaCambiada.slice(0,cociente);
      segundaMitad=cadenaCambiada.slice(cociente,(cadenaCambiada.length+1));
      inverso=segundaMitad+primeraMitad;
    }
    console.log('primera mitad',primeraMitad);
    console.log('segunda mitad',segundaMitad);
    console.log('inverso',inverso);
    console.log('cadena cambiada length',cadenaCambiada.length);
    console.log('inverso length',inverso.length);
    
    let reverso=inverso.split("").reverse().join("");
    console.log('reverso',reverso);
    
    let respuesta={
      "data_original":jsonData,
      "iguales":iguales,
      "base64_data":base64JsonData,
      "cadena_aleatoria":cadenaAleatoria,
      "caracteres_reemplazados":caracteresReemplazados,
      "nueva_cadena_json_sin_caracteres":nuevaBaseJsonData,
      "nueva_cadena_json_con_caracteres":cadenaCambiada,
      "inverso":inverso,
      "prmera_mitad":primeraMitad,
      "segunda_mitad":segundaMitad,
      "longitud_primera_mitas":primeraMitad.length,
      "longitud_segunda_mitas":segundaMitad.length,
      "reverso":reverso,
      "longitud_base64":base64JsonData.length,
      "longitud_cadenaCambiada":cadenaCambiada.length,
      "longitud_caracteres_generados":longitud2,
      "modulo":0
    }
    console.log(respuesta);
    
  }
  encriptarModulo1(jsonData,iguales,base64JsonData,cadenaAleatoria)
  {
    let caracteresReemplazados:string=""; //son los caracteres cada 3
    let nuevaBaseJsonData=""; //cadena sin los caracteres multiples de 3
    let longitud=base64JsonData.length;

    let indice=0;
    for (let i = 0; i < longitud; i++) {
      indice=i+1;
      if(indice%3==0) //es multiplo de 3
      {
        caracteresReemplazados=caracteresReemplazados+base64JsonData.charAt(i); //concatenamos
      }else{
        nuevaBaseJsonData=nuevaBaseJsonData+base64JsonData.charAt(i); //concatenamos
      } 
    }

    let cadenaCambiada="";
    let aux1=0;
    let aux2=2;
    let parar=0;
    let longitud2=cadenaAleatoria.length;

    //creamos la cadena cambiada
    for (let i = 0; i < longitud2; i++) {
      if (parar==(longitud2-1)) {
        let slice=nuevaBaseJsonData.substr((nuevaBaseJsonData.length-1),1); //tomo el ultimo caracter
        let add=slice+cadenaAleatoria.charAt(i); //al slice le agragamos el caracter random
        cadenaCambiada=cadenaCambiada+add; //concatenamos
        break;
      } else {
        let slice=nuevaBaseJsonData.slice(aux1,aux2) //tomamos cada dos (0,2) (2,4)...
        let add=slice+cadenaAleatoria.charAt(i); //al slice le agragamos el caracter random
        cadenaCambiada=cadenaCambiada+add; //concatenamos
        aux1=aux2;
        aux2=aux2+2;
        parar=parar+1;
      }
    }

    console.log('caracteresReemplazo',caracteresReemplazados);
    console.log('nuevaBaseJsonData',nuevaBaseJsonData);
    console.log('cadenaCambiada',cadenaCambiada);
    
    //partimos la cadena y cambiamos lugares
    let inverso="";
    let primeraMitad="";
    let segundaMitad="";
    let cociente=cadenaCambiada.length/2;
    cociente=Math.floor(cociente);

    if ((cadenaCambiada.length)%2==0) {
      primeraMitad=cadenaCambiada.slice(0,cociente);
      segundaMitad=cadenaCambiada.slice(cociente,cadenaCambiada.length);
      inverso=segundaMitad+primeraMitad;
    } else {
      primeraMitad=cadenaCambiada.slice(0,cociente);
      segundaMitad=cadenaCambiada.slice(cociente,(cadenaCambiada.length+1));
      inverso=segundaMitad+primeraMitad;
    }
    console.log('primera mitad',primeraMitad);
    console.log('segunda mitad',segundaMitad);
    console.log('inverso',inverso);
    console.log('cadena cambiada length',cadenaCambiada.length);
    console.log('inverso length',inverso.length);
    
    let reverso=inverso.split("").reverse().join("");
    console.log('reverso',reverso);
    
    let respuesta={
      "data_original":jsonData,
      "iguales":iguales,
      "base64_data":base64JsonData,
      "cadena_aleatoria":cadenaAleatoria,
      "caracteres_reemplazados":caracteresReemplazados,
      "nueva_cadena_json_sin_caracteres":nuevaBaseJsonData,
      "nueva_cadena_json_con_caracteres":cadenaCambiada,
      "inverso":inverso,
      "prmera_mitad":primeraMitad,
      "segunda_mitad":segundaMitad,
      "longitud_primera_mitas":primeraMitad.length,
      "longitud_segunda_mitas":segundaMitad.length,
      "reverso":reverso,
      "longitud_base64":base64JsonData.length,
      "longitud_cadenaCambiada":cadenaCambiada.length,
      "longitud_caracteres_generados":longitud2,
      "modulo":1
    }
    console.log(respuesta);
  }
  encriptarModulo2(jsonData,iguales,base64JsonData,cadenaAleatoria)
  {
    let caracteresReemplazados:string=""; //son los caracteres cada 3
    let nuevaBaseJsonData=""; //cadena sin los caracteres multiples de 3
    let longitud=base64JsonData.length;

    let indice=0;
    for (let i = 0; i < longitud; i++) {
      indice=i+1;
      if(indice%3==0) //es multiplo de 3
      {
        caracteresReemplazados=caracteresReemplazados+base64JsonData.charAt(i); //concatenamos
      }else{
        nuevaBaseJsonData=nuevaBaseJsonData+base64JsonData.charAt(i); //concatenamos
      } 
    }

    let cadenaCambiada="";
    let aux1=0;
    let aux2=2;
    let longitud2=cadenaAleatoria.length;

    //creamos la cadena cambiada
    for (let i = 0; i < longitud2; i++) {
      let slice=nuevaBaseJsonData.slice(aux1,aux2) //tomamos cada dos (0,2) (2,4)...
      let add=slice+cadenaAleatoria.charAt(i); //al slice le agragamos el caracter random
      cadenaCambiada=cadenaCambiada+add; //concatenamos
      aux1=aux2;
      aux2=aux2+2;
    }

    console.log('caracteresReemplazo',caracteresReemplazados);
    console.log('nuevaBaseJsonData',nuevaBaseJsonData);
    console.log('cadenaCambiada',cadenaCambiada);
    
    //partimos la cadena y cambiamos lugares
    let inverso="";
    let primeraMitad="";
    let segundaMitad="";
    let cociente=cadenaCambiada.length/2;
    cociente=Math.floor(cociente);

    if ((cadenaCambiada.length)%2==0) {
      primeraMitad=cadenaCambiada.slice(0,cociente);
      segundaMitad=cadenaCambiada.slice(cociente,cadenaCambiada.length);
      inverso=segundaMitad+primeraMitad;
    } else {
      primeraMitad=cadenaCambiada.slice(0,cociente);
      segundaMitad=cadenaCambiada.slice(cociente,(cadenaCambiada.length+1));
      inverso=segundaMitad+primeraMitad;
    }
    console.log('primera mitad',primeraMitad);
    console.log('segunda mitad',segundaMitad);
    console.log('inverso',inverso);
    console.log('cadena cambiada length',cadenaCambiada.length);
    console.log('inverso length',inverso.length);
    
    let reverso=inverso.split("").reverse().join("");
    console.log('reverso',reverso);
    
    let respuesta={
      "data_original":jsonData,
      "iguales":iguales,
      "base64_data":base64JsonData,
      "cadena_aleatoria":cadenaAleatoria,
      "caracteres_reemplazados":caracteresReemplazados,
      "nueva_cadena_json_sin_caracteres":nuevaBaseJsonData,
      "nueva_cadena_json_con_caracteres":cadenaCambiada,
      "inverso":inverso,
      "prmera_mitad":primeraMitad,
      "segunda_mitad":segundaMitad,
      "longitud_primera_mitas":primeraMitad.length,
      "longitud_segunda_mitas":segundaMitad.length,
      "reverso":reverso,
      "longitud_base64":base64JsonData.length,
      "longitud_cadenaCambiada":cadenaCambiada.length,
      "longitud_caracteres_generados":longitud2,
      "modulo":2
    }
    console.log(respuesta);
  }

  desencriptar()
  {
    let reverso:string,caracteresReemplazados:string,cadenaAleatoria:string,modulo:number,iguales:number;
    reverso='iONUmIi1XbuWye29ziNxR3LpJGajF';
    caracteresReemplazados='J2ciJHcvJ';
    cadenaAleatoria='WXINFGLxz2';
    modulo=1;
    iguales=0;

    let inverso:string;
    inverso=reverso.split("").reverse().join("");

    let primeraMitad:string="";
    let segundaMitad:string="";
    let cociente;

    if (modulo==0) {

      cociente=(inverso.length)/2;
      cociente=Math.floor(cociente);
      if ((inverso.length)%2==0) {
        primeraMitad=inverso.slice(0,cociente);
        segundaMitad=inverso.slice(cociente,inverso.length);
        inverso=segundaMitad+primeraMitad;
      } else {
        primeraMitad=inverso.slice(0,(cociente+1));
        segundaMitad=inverso.slice((cociente+1),inverso.length);
        inverso=segundaMitad+primeraMitad;
      }

      let caracteresReemplazadosDC:string = "";
      let nuevaBaseJsonDataDC:string = "";
      let longitud:number=inverso.length;

      let indice=0;
      for (let i = 0; i < longitud; i++) {
        indice=i+1;
        if(indice%3==0) //es multiplo de 3
        {
          caracteresReemplazadosDC=caracteresReemplazadosDC+inverso.charAt(i); //concatenamos
        }else{
          nuevaBaseJsonDataDC=nuevaBaseJsonDataDC+inverso.charAt(i); //concatenamos
        } 
      }

      if (caracteresReemplazadosDC==cadenaAleatoria) { //comprobacion
        let cadenaCambiada:string="";
        let aux1=0;
        let aux2=2;
        let longitud2=caracteresReemplazados.length;

        //creamos la cadena cambiada
        for (let i = 0; i < longitud2; i++) {
          let slice=nuevaBaseJsonDataDC.slice(aux1,aux2) //tomamos cada dos (0,2) (2,4)...
          let add=slice+caracteresReemplazados.charAt(i); //al slice le agragamos el caracter random
          cadenaCambiada=cadenaCambiada+add; //concatenamos
          aux1=aux2;
          aux2=aux2+2;
        }

        //ponemos los iguales
        for (let i = 0; i < iguales; i++) {
          cadenaCambiada=cadenaCambiada+"=";
        }

        let resultado=JSON.parse(atob(cadenaCambiada));
        console.log(resultado);
        
      } else {
        console.log('error no se pudo codificar');
        
      }

      
    } else {
      //console.log(inverso);
      
      cociente=(inverso.length)/2;
      cociente=Math.floor(cociente);
      if ((inverso.length)%2==0) {
        primeraMitad=inverso.slice(0,cociente);
        segundaMitad=inverso.slice(cociente,inverso.length);
        inverso=segundaMitad+primeraMitad;
      } else {
        primeraMitad=inverso.slice(0,(cociente+1));
        segundaMitad=inverso.slice((cociente+1),inverso.length);
        inverso=segundaMitad+primeraMitad;
      }

      if (modulo==1) {
        
        let caracteresReemplazadosDC:string = "";
        let nuevaBaseJsonDataDC:string = "";
        let longitud:number=inverso.length;
        
        let indice=0;
        for (let i = 0; i < longitud; i++) {
          indice=i+1;
          if(indice%3==0) //es multiplo de 3
          {
            caracteresReemplazadosDC=caracteresReemplazadosDC+inverso.charAt(i); //concatenamos
          }else{
            nuevaBaseJsonDataDC=nuevaBaseJsonDataDC+inverso.charAt(i); //concatenamos
          } 
        }

        let ultimo_caracter=inverso.substr((inverso.length-1),1);        
        caracteresReemplazadosDC=caracteresReemplazadosDC+ultimo_caracter;
        
        if (caracteresReemplazadosDC==cadenaAleatoria) { //comprobacion
          
          nuevaBaseJsonDataDC=nuevaBaseJsonDataDC.substr(0,(nuevaBaseJsonDataDC.length-1)); //elimino ultimo caracter
          
          let ultima_letra=nuevaBaseJsonDataDC.substr((nuevaBaseJsonDataDC.length-1),1);
          let cadenaCambiada:string="";
          let aux1=0;
          let aux2=2;
          let longitud2=caracteresReemplazados.length;

          //creamos la cadena cambiada
          for (let i = 0; i < longitud2; i++) {
            let slice=nuevaBaseJsonDataDC.slice(aux1,aux2) //tomamos cada dos (0,2) (2,4)...
            let add=slice+caracteresReemplazados.charAt(i); //al slice le agragamos el caracter random
            cadenaCambiada=cadenaCambiada+add; //concatenamos
            aux1=aux2;
            aux2=aux2+2;
          }
          cadenaCambiada=cadenaCambiada+ultima_letra;
          //console.log(cadenaCambiada);
          
          //ponemos los iguales
          for (let i = 0; i < iguales; i++) {
            cadenaCambiada=cadenaCambiada+"=";
          }

          let resultado=JSON.parse(atob(cadenaCambiada));
          console.log(resultado);
          
        } else {
          console.log('error no se pudo codificar');
          
        }

      } else {
        //modulo 2
        let caracteresReemplazadosDC:string = "";
        let nuevaBaseJsonDataDC:string = "";
        let longitud:number=inverso.length;

        let indice=0;
        for (let i = 0; i < longitud; i++) {
          indice=i+1;
          if(indice%3==0) //es multiplo de 3
          {
            caracteresReemplazadosDC=caracteresReemplazadosDC+inverso.charAt(i); //concatenamos
          }else{
            nuevaBaseJsonDataDC=nuevaBaseJsonDataDC+inverso.charAt(i); //concatenamos
          } 
        }

        
        if (caracteresReemplazadosDC==cadenaAleatoria) { //comprobacion
          
          let ultimas_letras=nuevaBaseJsonDataDC.substr((nuevaBaseJsonDataDC.length-2),2);
          let cadenaCambiada:string="";
          let aux1=0;
          let aux2=2;
          let longitud2=caracteresReemplazados.length;

          //creamos la cadena cambiada
          for (let i = 0; i < longitud2; i++) {
            let slice=nuevaBaseJsonDataDC.slice(aux1,aux2) //tomamos cada dos (0,2) (2,4)...
            let add=slice+caracteresReemplazados.charAt(i); //al slice le agragamos el caracter random
            cadenaCambiada=cadenaCambiada+add; //concatenamos
            aux1=aux2;
            aux2=aux2+2;
          }
          cadenaCambiada=cadenaCambiada+ultimas_letras;
          //ponemos los iguales
          for (let i = 0; i < iguales; i++) {
            cadenaCambiada=cadenaCambiada+"=";
          }

          let resultado=JSON.parse(atob(cadenaCambiada));
          console.log(resultado);
          
        } else {
          console.log('error no se pudo codificar');
          
        }
      }
    }
  }

}
