import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { NavbarService } from 'app/services/navbar.service';
import { ParametersService } from 'app/services/parameters.service';
import { PresentersService } from 'app/services/presenters.service';
import { AdministradorServiceService } from 'app/services/administrador-service.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-registro-admin',
  templateUrl: './registro-admin.component.html',
  styleUrls: ['./registro-admin.component.css']
})
export class RegistroAdminComponent implements OnInit {

  form:FormGroup;
  constructor(public nav:NavbarService,
    public parameters:ParametersService,
    public presenters:PresentersService,
    public aservice:AdministradorServiceService,
    public route:Router,
    public spinner:NgxSpinnerService) { 
      this.form= new FormGroup({
        'nombre': new FormControl('',[Validators.required]),
        'apellidos': new FormControl('',[Validators.required]),
        'carnet': new FormControl('',[Validators.required]),
        'correo': new FormControl('',[Validators.required, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$')]),
        'telefono': new FormControl('',[Validators.required]),
        'fecha_nacimiento': new FormControl('',[Validators.required])
      });
      
    }

  ngOnInit() {
    this.nav.show();
  }

  registrar()
  {
    console.log(this.form.value);
    let info:any;
    this.spinner.show();
    let token=sessionStorage.getItem('token');
    this.aservice.registrarAdministrador(token,this.form.value).subscribe(
      (data)=>{
        this.spinner.hide();
        data=data.json(); //parse to json
        info=Object.assign(data);
        console.log('info',info);
        
        if(info.estado=="exito")
        {
          //exito
          this.presenters.presentAlertSuccess('Se registro el administrador correctamente.');
          this.route.navigate(['/administradores']);
        }else{
          //error
          if(info.mensaje=="el token ha expirado."){
            //mandar a login
            this.presenters.presentAlertInfo('La sesion expiro','Por favor vuelva a ingresar.');
            this.parameters.setToken('');
            this.parameters.setRol('');
            sessionStorage.removeItem('token');
            sessionStorage.removeItem('rol');
            this.route.navigate(['/login']);
          }else{
            //imprimir mensaje
            this.presenters.presentAlertFail(info.mensaje);
          }
        }

      },(error)=>{
        //error en la conexion
        this.spinner.hide();
        this.presenters.presentAlertFail('No hay conexion con el servidor, intente mas tarde.');
      }
    );
    
  }

}
