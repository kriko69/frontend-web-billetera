import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { NavbarService } from '../../services/navbar.service';
import { ParametersService } from 'app/services/parameters.service';
import { AdministradorServiceService } from 'app/services/administrador-service.service';
import { PresentersService } from 'app/services/presenters.service';
import { NgxSpinnerService } from 'ngx-spinner';
import  Swal  from 'sweetalert2';
import { analyzeAndValidateNgModules } from '@angular/compiler';


@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html',
  styleUrls: ['./usuarios.component.css']
})
export class UsuariosComponent implements OnInit {

  usuarios:any;
  cantidad:number;
  nombreUsuario:string='';
  carnet:number;
  copia_usuarios_original:any;
  page:number=1;
  constructor(public nav:NavbarService,
    public route:Router,
    public parameters:ParametersService,
    public aservice:AdministradorServiceService,
    public presenters:PresentersService,
    public spinner:NgxSpinnerService) { 
      
      console.log('token desde usuarios',sessionStorage.getItem('token'));
      console.log('rol desde usuarios',sessionStorage.getItem('rol'));
      
  }

  ngOnInit() {
    this.nav.show();
    this.nav.rol=sessionStorage.getItem('rol');
    this.obtenerPerfilNombre();
    this.obtenerListaUsuarios();
  }

  irRegistro()
  {
    this.parameters.setTipoRegistroUsuario("estudiante");
    this.route.navigate(['/registro']);
  }

  editar(usuario)
  {
    this.parameters.setTipoRegistroUsuario("estudiante");
    this.parameters.setEstudianteEditar(usuario);
    this.route.navigate(['/registro']);
  }

  eliminar(usuario)
  {
    console.log(usuario.usuario_id);
    let usuario_id=usuario.usuario_id;
    let token=sessionStorage.getItem('token');
    let info:any;
    this.spinner.show();
    this.aservice.eliminarUsuario(token,usuario_id).subscribe(
      (data)=>{
        this.spinner.hide();
        data=data.json();
        info=Object.assign(data);
        console.log('info',info);
        
        if(info.estado=="exito")
        {
          //exito
          this.presenters.presentAlertSuccess('Se eliminao al usuario correctamente');
          //actualizo page
          setTimeout(()=>{
            window.location.reload();
          },1000);
        }else{
          //error
          //imprimir mensaje
          if(info.mensaje=="el token ha expirado."){
            //mandar a login
            this.presenters.presentAlertInfo('La sesion expiro','Por favor vuelva a ingresar.');
            this.parameters.setToken('');
            this.parameters.setRol('');
            sessionStorage.removeItem('token');
            sessionStorage.removeItem('rol');
            this.route.navigate(['/login']);
          }else{
            //imprimir mensaje
            this.presenters.presentAlertFail(info.mensaje);
          }
        }
      },(error)=>{
        this.spinner.hide();
        this.presenters.presentAlertFail('No hay conexion con el servidor, intente mas tarde.');
      }
    );
  }

  obtenerPerfilNombre()
  {
    let info:any;
    this.spinner.show();
    let token=sessionStorage.getItem('token');
    this.aservice.perfil(token).subscribe(
      (data)=>{
        this.spinner.hide();
        data=data.json(); //parse to json
        info=Object.assign(data);
        console.log('info',info);
        
        if(info.estado=="exito")
        {
          //exito
          this.parameters.setUsuarioLogin(info.data);
          this.nombreUsuario=info.data["nombre"]+' '+info.data["apellidos"];
          console.log('usuario:',this.nombreUsuario);                    
          if(this.parameters.getInicioSesion())
          {
            this.presenters.presentAlertSuccess('Bienvenido '+this.nombreUsuario);
            this.parameters.setInicioSesion(false);
          }
          if(info.data["password_default"])
          {
            //si la contrasena es por defecto
            this.alertaCambioPassword(token);
          }
          
        }else{
          //error
          if(info.mensaje=="el token ha expirado."){
            //mandar a login
            this.presenters.presentAlertInfo('La sesion expiro','Por favor vuelva a ingresar.');
            this.parameters.setToken('');
            this.parameters.setRol('');
            sessionStorage.removeItem('token');
            sessionStorage.removeItem('rol');
            this.route.navigate(['/login']);
          }else{
            //imprimir mensaje
            this.presenters.presentAlertFail(info.mensaje);
          }
        }

      },(error)=>{
        //error en la conexion
        this.spinner.hide();
        this.presenters.presentAlertFail('No hay conexion con el servidor, intente mas tarde.');
      }
    );
  }

  obtenerListaUsuarios()
  {
    let info:any;
    this.spinner.show();
    let token=sessionStorage.getItem('token');
    this.aservice.obtenerListaUsuarios(token).subscribe(
      (data)=>{
        this.spinner.hide();
        data=data.json(); //parse to json
        info=Object.assign(data);
        console.log('info',info);
        if(info.estado=="exito")
        {
          //exito
          if(info.data==null)
          {
            this.usuarios=[];
            this.cantidad=0
          }else{
            this.usuarios=info.data;
            this.copia_usuarios_original=this.usuarios;
            this.cantidad=this.usuarios.length;
            console.log('usuarios:',this.usuarios);
            
          }
        }else{
          //error
          if(info.mensaje=="el token ha expirado."){
            //mandar a login
            this.presenters.presentAlertInfo('La sesion expiro','Por favor vuelva a ingresar.');
            this.parameters.setToken('');
            this.parameters.setRol('');
            sessionStorage.removeItem('token');
            sessionStorage.removeItem('rol');
            this.route.navigate(['/login']);
          }else{
            //imprimir mensaje
            this.presenters.presentAlertFail(info.mensaje);
          }
        }
      },(error)=>{
        //error en la conexion
        this.spinner.hide();
        this.presenters.presentAlertFail('No hay conexion con el servidor, intente mas tarde.');
      }
    );
  }
  copia_usuarios:any;
  resultados:any=[];
  buscar()
  {
    console.log(this.carnet);
    if(this.carnet==null){
      console.log(this.copia_usuarios);
      
      this.usuarios=this.copia_usuarios_original;
      this.cantidad=this.copia_usuarios_original.length;
    }else{
      setTimeout(()=>{
        this.copia_usuarios=this.copia_usuarios_original;
        console.log(this.copia_usuarios);
        
        for (let i = 0; i < this.copia_usuarios.length; i++) {
          let carn=this.copia_usuarios[i].carnet;
          if(carn==this.carnet)
          {
            this.resultados.push(this.copia_usuarios[i]);
          } 
        }
        
        this.cantidad=this.resultados.length;
        this.usuarios=this.resultados;
        console.log(this.usuarios);
        this.resultados=[];
      },1000);
    }
    
  }

  /**
  filtrarPorNombre(palabra:string)
  {
    let nombre = palabra.toUpperCase();
    let resultados=[];
    this.todosUsuario=[];
    this.obtenerTodosLosUsuarios();
    setTimeout(() => {
      console.log('TODOS',this.todosUsuario);
      for (let i = 0; i < this.todosUsuario.length; i++) {
        let nombre2:string=this.todosUsuario[i].nombre;      
        if(nombre2.toUpperCase().search(nombre)>-1)
        {
          resultados.push(this.todosUsuario[i]);
        }
      }
      this.usuarios=resultados;
    }, 2000);
  }
   */

  alertaCambioPassword(token)
  {
    let info:any;
    Swal.fire({
      title: 'Por seguridad cambie su password por defecto.',
      input: 'text',
      inputPlaceholder: 'Ingrese nueva password',
      inputAttributes: {
        autocapitalize: 'off'
      },
      showCancelButton: true,
      confirmButtonText: 'Cambiar password',
      preConfirm:(value)=>{
        let es_valido=this.validarPassword(value);
        if(!es_valido){
          this.showRules(token);
        }else{
          return value;
        }
      },
      showLoaderOnConfirm: true,
      allowOutsideClick: () => !Swal.isLoading()
    }).then((result) => {
      if (result.value) {
        console.log(result.value);
        
        this.aservice.cambiarPasswordDefault(token,result.value).subscribe(
          (data)=>{
            data=data.json(); //parse to json
            info=Object.assign(data);
            console.log('info',info);
            if(info.estado=="exito")
            {
              //exito
              this.presenters.presentAlertSuccess("Se actualizo el password correctamente.");
            }else{
              //error
              if(info.mensaje=="el token ha expirado."){
                //mandar a login
                this.presenters.presentAlertInfo('La sesion expiro','Por favor vuelva a ingresar.');
                this.parameters.setToken('');
                this.parameters.setRol('');
                sessionStorage.removeItem('token');
                sessionStorage.removeItem('rol');
                this.route.navigate(['/login']);
              }else{
                //imprimir mensaje
                this.presenters.presentAlertFail(info.mensaje);
              }
            }
          },(error)=>{
            //error en la conexion
            this.presenters.presentAlertFail('No hay conexion con el servidor, intente mas tarde.');
          }
        );
        
        
      }
    })  
  }

  preguntar(usuario)
  {
    Swal.fire({
      title: "¿Esta seguro de que el o la estudiante "+usuario.nombre+" "+usuario.apellidos+" termino su carrera universitaria?",
      text: "Una vez retirado debe entregar el dinero.",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, hazlo!'
    }).then((result) => {
      if (result.value) {
        this.retirar(usuario);
      }
    });
  }
  retirar(usuario)
  {
    console.log('usuario',usuario);
    
  }

  validarPassword(password:string)
  {
    // se pone /expresion_regular/.test(palabra a examinar) y devuelve un booleano
    return /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{10,}$/.test(password);
  }

  showRules(token)
  {
    Swal.fire({
      title: "El password no cumple con los requisitos requeridos.",
      html: '<p>Debe tener al menos:</p>'+
              '<ul>'+
                '<li>10 caracters.</li>'+
                '<li>1 mayuscula.</li>'+
                '<li>1 minuscula.</li>'+
                '<li>1 numero.</li>'+
                '<li>1 caracter especial.</li>'+
                '(@$!%*?&)'+
              '</ul>',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Cambiar password'
    }).then((result) => {
      if (result.value) {
        this.alertaCambioPassword(token);
      }
    });
  }
}
