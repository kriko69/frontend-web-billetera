import { ParametersService } from './../../services/parameters.service';
import { Component, OnInit } from '@angular/core';
import { NavbarService } from 'app/services/navbar.service';
import { NgForm, FormControl, FormGroup,Validators, Form } from '@angular/forms';
import { PresentersService } from 'app/services/presenters.service';
import { AdministradorServiceService } from 'app/services/administrador-service.service';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { isNullOrUndefined } from 'util';

@Component({
  selector: 'app-editar-mensualidad',
  templateUrl: './editar-mensualidad.component.html',
  styleUrls: ['./editar-mensualidad.component.css']
})
export class EditarMensualidadComponent implements OnInit {

  form:FormGroup;
  gestion_padre;
  mensualidad;
  ver=true;
  constructor(public nav:NavbarService,
    public parameters:ParametersService,
    public presenters:PresentersService,
    public aservice:AdministradorServiceService,
    public route:Router,
    public spinner:NgxSpinnerService) { 
    this.form= new FormGroup({
      'nombre': new FormControl({value:'',disabled:this.ver},[Validators.required]),
      'fecha_pago': new FormControl({value:'',disabled:this.ver},[Validators.required]),
      'monto': new FormControl({value:'',disabled:this.ver},[Validators.required]),
      'mensualidad_id': new FormControl('',[Validators.required])
      
    });
  }

  ngOnInit() {
    this.nav.show();
    this.mensualidad=this.parameters.getMensualidad();
    this.gestion_padre=this.parameters.getGestion();
    this.cargarDatosMensualidad();
  }

  cargarDatosMensualidad()
  {
    if(isNullOrUndefined(this.mensualidad))
    {
      this.route.navigate(['/gestiones']);
    }
    console.log(this.mensualidad);
    
    let fecha=this.mensualidad.fecha_pago;
    console.log(fecha);
    let resFecha = fecha.split("-");
    let reversedFecha = resFecha.reverse();
    fecha=reversedFecha[0]+'-'+reversedFecha[1]+'-'+reversedFecha[2];
    this.form.patchValue({
      'nombre': this.mensualidad.nombre,
      'fecha_pago': fecha,
      'monto': this.mensualidad.monto,
      'mensualidad_id': this.mensualidad.mensualidad_id,
      
    });
    console.log(this.ver);
    
  }

  atras()
  {
    this.route.navigate(['/ver-mensualidad']);
  }

  editarDatos()
  {
    this.ver=!this.ver;
    if(!this.ver)
    {
      this.form.controls['nombre'].enable();
      this.form.controls['fecha_pago'].enable();
      this.form.controls['monto'].enable();
    }else{
      this.form.controls['nombre'].disable();
      this.form.controls['fecha_pago'].disable();
      this.form.controls['monto'].disable();
    }
  }

  editar()
  {
    let fecha=this.form.value.fecha_pago;
    console.log(fecha);
    let resFecha = fecha.split("-");
    let reversedFecha = resFecha.reverse();
    fecha=reversedFecha[0]+'-'+reversedFecha[1]+'-'+reversedFecha[2];
    this.form.patchValue({
      'fecha_pago': fecha
    });
    console.log(this.form.value);
    
    
      let token=sessionStorage.getItem('token');
      let info:any;
      this.spinner.show();
      this.aservice.editarMensualidad(token,this.form.value).subscribe(
        (data)=>{
          this.spinner.hide();
          data=data.json();
          info=Object.assign(data);
          console.log('info',info);
          
          if(info.estado=="exito")
          {
            //exito
            this.presenters.presentAlertSuccess('Se actualizo la mensualidad correctamente');
            setTimeout(()=>{
              this.route.navigate(['/gestiones']);
            },1500);
          }else{
            //error
            //imprimir mensaje
            if(info.mensaje=="el token ha expirado."){
              //mandar a login
              this.presenters.presentAlertInfo('La sesion expiro','Por favor vuelva a ingresar.');
              this.parameters.setToken('');
              this.parameters.setRol('');
              sessionStorage.removeItem('token');
              sessionStorage.removeItem('rol');
              this.route.navigate(['/login']);
            }else{
              //imprimir mensaje
              this.presenters.presentAlertFail(info.mensaje);
            }
          }
        },(error)=>{
          this.spinner.hide();
          this.presenters.presentAlertFail('No hay conexion con el servidor, intente mas tarde.');
        }
      );
  }

}
