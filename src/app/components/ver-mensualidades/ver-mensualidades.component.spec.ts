import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VerMensualidadesComponent } from './ver-mensualidades.component';

describe('VerMensualidadesComponent', () => {
  let component: VerMensualidadesComponent;
  let fixture: ComponentFixture<VerMensualidadesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VerMensualidadesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerMensualidadesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
