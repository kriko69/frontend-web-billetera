import { NavbarService } from 'app/services/navbar.service';
import { Component, OnInit } from '@angular/core';
import { AdministradorServiceService } from 'app/services/administrador-service.service';
import { ParametersService } from 'app/services/parameters.service';
import { PresentersService } from 'app/services/presenters.service';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import  Swal  from 'sweetalert2';
import { isNullOrUndefined } from 'util';

@Component({
  selector: 'app-ver-mensualidades',
  templateUrl: './ver-mensualidades.component.html',
  styleUrls: ['./ver-mensualidades.component.css']
})
export class VerMensualidadesComponent implements OnInit {

  nombre_mensualidad;
  mensualidades;
  cantidad;
  gestion_padre;
  copia_mensualidades_original;
  page:number=1;
  constructor(
    public navbar:NavbarService,
    public route:Router,
    public aservice:AdministradorServiceService,
    public parameters:ParametersService,
    public presenters:PresentersService,
    public spinner:NgxSpinnerService
    ) {

   }

  ngOnInit() {
    this.navbar.show();
    this.gestion_padre=this.parameters.getGestion();
    this.obtenerMensualidades();
  }

  obtenerMensualidades()
  {
    if(isNullOrUndefined(this.gestion_padre))
    {
      this.route.navigate(['/gestiones']);
    }
    let gestion_id=this.gestion_padre.gestion_id;
    let info:any;
    this.spinner.show();
    let token=sessionStorage.getItem('token');
    this.aservice.listarMensualidades(token,gestion_id).subscribe(
      (data)=>{
        this.spinner.hide();
        data=data.json(); //parse to json
        info=Object.assign(data);
        console.log('info',info);
        if(info.estado=="exito")
        {
          //exito
          if(info.data==null)
          {
            this.mensualidades=[];
            this.cantidad=0
          }else{
            this.mensualidades=info.data;
            this.copia_mensualidades_original=this.mensualidades;
            this.cantidad=this.mensualidades.length;
            console.log('usuarios:',this.mensualidades);
            
          }
        }else{
          //error
          if(info.mensaje=="el token ha expirado."){
            //mandar a login
            this.presenters.presentAlertInfo('La sesion expiro','Por favor vuelva a ingresar.');
            this.parameters.setToken('');
            this.parameters.setRol('');
            sessionStorage.removeItem('token');
            sessionStorage.removeItem('rol');
            this.route.navigate(['/login']);
          }else{
            //imprimir mensaje
            this.presenters.presentAlertFail(info.mensaje);
          }
        }
      },(error)=>{
        //error en la conexion
        this.spinner.hide();
        this.presenters.presentAlertFail('No hay conexion con el servidor, intente mas tarde.');
      }
    );
    
  }

  atras()
  {
    this.route.navigate(['/gestiones']);
  }

  editarMensualidad(mensualidad)
  {
    this.parameters.setMensualidad(mensualidad);
    this.route.navigate(['/editar-mensualidad']);
  }

  verPagos(mensualidad)
  {
    this.parameters.setMensualidad(mensualidad);
    this.route.navigate(['/pagos-mensualidad']);
  }

  copia_mensualidades:any;
  resultados:any=[];
  buscar()
  {
    if(this.nombre_mensualidad==null || this.nombre_mensualidad==''){
      
      this.mensualidades=this.copia_mensualidades_original;
      this.cantidad=this.copia_mensualidades_original.length;
    }else{
      setTimeout(()=>{
        this.copia_mensualidades=this.copia_mensualidades_original;
        console.log(this.copia_mensualidades);
        
        for (let i = 0; i < this.copia_mensualidades.length; i++) {
          let nombre=this.copia_mensualidades[i].nombre;
          if(nombre==this.nombre_mensualidad)
          {
            this.resultados.push(this.copia_mensualidades[i]);
          } 
        }
        
        this.cantidad=this.resultados.length;
        this.mensualidades=this.resultados;
        console.log(this.mensualidades);
        this.resultados=[];
      },1000);
    }
  }

}
