import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { APP_ROUTING } from './app.routes';
import { HttpClientModule } from '@angular/common/http';
import { MenuComponent } from './components/menu/menu.component';

import {NavbarService  } from "../app/services/navbar.service";
import { UsuariosComponent } from './components/usuarios/usuarios.component';
import { RegistroComponent } from './components/registro/registro.component';
import { AdminsComponent } from './components/admins/admins.component';
import {ParametersService  } from "../app/services/parameters.service";
import { CuentasComponent } from './components/cuentas/cuentas.component';
import { RetiroComponent } from './components/retiro/retiro.component';
import { DepositoComponent } from './components/deposito/deposito.component';
import { AfiliacionComponent } from './components/afiliacion/afiliacion.component';
import { PerfilComponent } from './components/perfil/perfil.component';
import { CambioPasswordComponent } from './components/cambio-password/cambio-password.component';
import { PresentersService } from './services/presenters.service';
import { RecuperarPasswordComponent } from './components/recuperar-password/recuperar-password.component';
import { NuevaPasswordComponent } from './components/nueva-password/nueva-password.component';
import { AdministradorServiceService } from './services/administrador-service.service';
import { RegistroAdminComponent } from './components/registro-admin/registro-admin.component';

import { NgxSpinnerModule } from "ngx-spinner";
import { NgxPaginationModule } from 'ngx-pagination';
import { GestionesComponent } from './components/gestiones/gestiones.component';
import { AgregarMensualidadComponent } from './components/agregar-mensualidad/agregar-mensualidad.component';
import { VerMensualidadesComponent } from './components/ver-mensualidades/ver-mensualidades.component';
import { RegistroGestionComponent } from './components/registro-gestion/registro-gestion.component';
import { EditarMensualidadComponent } from './components/editar-mensualidad/editar-mensualidad.component';
import { PagosMensualidadComponent } from './components/pagos-mensualidad/pagos-mensualidad.component';
import { PagoGestionesComponent } from './components/pago-gestiones/pago-gestiones.component';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    MenuComponent,
    UsuariosComponent,
    RegistroComponent,
    AdminsComponent,
    CuentasComponent,
    RetiroComponent,
    DepositoComponent,
    AfiliacionComponent,
    PerfilComponent,
    CambioPasswordComponent,
    RecuperarPasswordComponent,
    NuevaPasswordComponent,
    RegistroAdminComponent,
    GestionesComponent,
    AgregarMensualidadComponent,
    VerMensualidadesComponent,
    RegistroGestionComponent,
    EditarMensualidadComponent,
    PagosMensualidadComponent,
    PagoGestionesComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    APP_ROUTING,
    HttpClientModule,
    ReactiveFormsModule,
    NgxSpinnerModule,
    NgxPaginationModule
  ],
  providers: [
    NavbarService,
    ParametersService,
    PresentersService,
    AdministradorServiceService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
