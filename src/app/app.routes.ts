import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from "../app/components/login/login.component";
import { MenuComponent } from "../app/components/menu/menu.component";
import { UsuariosComponent } from "../app/components/usuarios/usuarios.component";
import { RegistroComponent } from "../app/components/registro/registro.component";
import { AdminsComponent } from "../app/components/admins/admins.component";
import { CuentasComponent } from "../app/components/cuentas/cuentas.component";
import { RetiroComponent } from "../app/components/retiro/retiro.component";
import { DepositoComponent } from "../app/components/deposito/deposito.component";
import { AfiliacionComponent } from "../app/components/afiliacion/afiliacion.component";
import { PerfilComponent } from "../app/components/perfil/perfil.component";
import { CambioPasswordComponent } from "../app/components/cambio-password/cambio-password.component";
import { RecuperarPasswordComponent } from "../app/components/recuperar-password/recuperar-password.component";
import { NuevaPasswordComponent } from "../app/components/nueva-password/nueva-password.component";
import { RegistroAdminComponent } from "../app/components/registro-admin/registro-admin.component";
import { GestionesComponent } from './components/gestiones/gestiones.component';
import { RegistroGestionComponent } from './components/registro-gestion/registro-gestion.component';
import { AgregarMensualidadComponent } from './components/agregar-mensualidad/agregar-mensualidad.component';
import { VerMensualidadesComponent } from './components/ver-mensualidades/ver-mensualidades.component';
import { EditarMensualidadComponent } from './components/editar-mensualidad/editar-mensualidad.component';
import { PagosMensualidadComponent } from './components/pagos-mensualidad/pagos-mensualidad.component';
import { PagoGestionesComponent } from './components/pago-gestiones/pago-gestiones.component';

const APP_ROUTER: Routes = [
    { path: 'login', component: LoginComponent},
    { path: 'menu', component: MenuComponent},
    { path: 'usuarios', component: UsuariosComponent},
    { path: 'registro', component: RegistroComponent},
    { path: 'administradores', component: AdminsComponent},
    { path: 'cuentas', component: CuentasComponent},
    { path: 'retiro', component: RetiroComponent},
    { path: 'deposito', component: DepositoComponent},
    { path: 'afiliacion', component: AfiliacionComponent},
    { path: 'perfil', component: PerfilComponent},
    { path: 'cambio-password', component: CambioPasswordComponent},
    { path: 'recuperar-password', component: RecuperarPasswordComponent},
    { path: 'nueva-password', component: NuevaPasswordComponent},
    { path: 'registro-admin', component: RegistroAdminComponent},
    { path: 'gestiones', component: GestionesComponent},
    { path: 'registro-gestion', component: RegistroGestionComponent},
    { path: 'agregar-mensualidad', component: AgregarMensualidadComponent},
    { path: 'ver-mensualidad', component: VerMensualidadesComponent},
    { path: 'editar-mensualidad', component: EditarMensualidadComponent},
    { path: 'pagos-mensualidad', component: PagosMensualidadComponent},
    { path: 'pagos-gestion', component: PagoGestionesComponent},
    { path: '**', redirectTo: '/login',  pathMatch: 'full' },
    { path: '', redirectTo: '/login', pathMatch: 'full' },

    //{ path: 'path/:routeParam', component: MyComponent },
    //{ path: 'staticPath', component: ... },
    //{ path: '**', component: ... },
    //{ path: 'oldPath', redirectTo: '/staticPath' },
    //{ path: ..., component: ..., data: { message: 'Custom' }
];


export const APP_ROUTING=RouterModule.forRoot(APP_ROUTER);
